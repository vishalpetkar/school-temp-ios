import { Injectable } from '@angular/core';
import {  HttpRequest,  HttpHandler,  HttpEvent,  HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProfileState } from '../store/profile.actions';
import { Select } from '@ngxs/store';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

  access_token = '';
  @Select(ProfileState.accesstokenKey) profile$: Observable<string>;
  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    this.profile$.subscribe(token => {
      this.access_token = token;
    });

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.access_token}`
      }
    });
    return next.handle(request);
  }
}
