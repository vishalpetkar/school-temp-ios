import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Profile } from 'src/app/models/school-model';
import { AddProfile, GetProfile, ProfileState } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.scss'],
})
export class CreatePasswordComponent implements OnInit {

  password: string;
  confirm_password: string;

  public tempUserData = new Profile();
  @Select(ProfileState.tempUserKey) tempprofile$: Observable<Profile>;

  constructor(
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.tempprofile$.subscribe(data => {
      this.tempUserData = data;
      console.log("Temp User Data", this.tempUserData);
    });
  }

  submit() {
    console.log("create password");
    this.tempUserData.password = this.password;
    this.tempUserData.confirm_password = this.confirm_password;
    this.store.dispatch(new AddProfile({data: this.tempUserData})).subscribe(success => {
      console.log("temp profileData", this.tempUserData);
      this.store.dispatch(new GetProfile({data: this.tempUserData})).subscribe(success => {
        if (this.tempUserData.type == 'parent') {
          this.router.navigate(['/search-children']);
        } else {
          this.router.navigate(['/campus-aonoymous']);
        }
      });
    });
  }
}
