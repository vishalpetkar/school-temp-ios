import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Profile } from 'src/app/models/school-model';
import { GetProfile } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public tempUserData = new Profile();
  email: string = '';
  password: string = '';

  constructor(private router: Router,
    private store: Store) { }

  ngOnInit() {}

  login() {
    this.tempUserData.email = this.email;
    this.tempUserData.password = this.password;
    this.store.dispatch(new GetProfile({data: this.tempUserData})).subscribe(success => {
      this.email = '';
      this.password = '';
      this.router.navigate(['/campus-aonoymous']);
    });
  }
}
