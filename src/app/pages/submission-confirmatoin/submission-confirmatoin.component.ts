import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { FriendsInfo } from 'src/app/models/friend-model';
import { StorageService } from 'src/app/services/storage.service';
import { KeepFriendsInfo } from 'src/app/store/profile.actions';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-submission-confirmatoin',
  templateUrl: './submission-confirmatoin.component.html',
  styleUrls: ['./submission-confirmatoin.component.scss'],
})
export class SubmissionConfirmatoinComponent implements OnInit {

  constructor(
    private storage: StorageService,
    private store: Store,
    private socialSharing: SocialSharing) { }

  ngOnInit() {
    this.storage.clearKey('friends_info');
    const friendsInfo = new FriendsInfo();
    this.store.dispatch(new KeepFriendsInfo({data: friendsInfo})).subscribe(success => {});
  }

}
