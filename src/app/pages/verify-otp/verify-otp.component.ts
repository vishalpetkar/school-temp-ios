import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Profile } from 'src/app/models/school-model';
import { ProfileState, SendOTP, TempProfile } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.scss'],
})
export class VerifyOtpComponent implements OnInit {

  @ViewChild('text1') text1 ;
  @ViewChild('text2') text2 ;
  @ViewChild('text3') text3 ;
  @ViewChild('text4') text4 ;

  public one = '';
  public two = '';
  public three = '';
  public four = '';

  public tempUserData = new Profile();
  public userData = new Profile();
  @Select(ProfileState.tempUserKey) tempprofile$: Observable<Profile>;
  @Select(ProfileState.profileKey) profile$: Observable<Profile>;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private store: Store) { }

  ngOnInit() {
    this.profile$.subscribe(data => {
      this.userData = data;
      console.log("User Data", this.userData);
    });

    this.tempprofile$.subscribe(data => {
      this.tempUserData = data;
      console.log("Temp User Data", this.tempUserData);
    });
  }

  async verify() {
    console.log("verify");
    // this.store.dispatch(new TempProfile({data: profileData})).subscribe(success => {});

    let otp = this.one.concat(this.two, this.three, this.four);

    if (this.userData.otp == otp) {
      if (this.tempUserData.action == 'forget') {
        this.router.navigate(['/update-password']);
      } else {
        this.router.navigate(['/create-password']);
      }
    } else {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Error',
        subHeader: '',
        message: "OTP is wrong.",
        buttons: ['OK']
      });
      await alert.present();
      this.text1.setFocus();
      this.one = '';
      this.two = '';
      this.three = '';
      this.four = '';
    }

  }

  clickEvent(fromtext, element) {

    var length = element.value.length;
    console.log('length', length);
    if (fromtext == 'text1' && length == 1) {
      this.text2.setFocus();
    } else if (fromtext == 'text2' && length == 1) {
      this.text3.setFocus();
    } else if (fromtext == 'text3' && length == 1) {
      this.text4.setFocus();
    }    
  }

  sendOtp() {
    this.store.dispatch(new SendOTP({data: this.tempUserData})).subscribe(success => {});
  }
}
