import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { ProfileState, KeepFriendsInfo } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-reward-option',
  templateUrl: './reward-option.component.html',
  styleUrls: ['./reward-option.component.scss'],
})
export class RewardOptionComponent implements OnInit {

  reward: string = '';
  @Select(ProfileState.tempFriendInfoKey) tempFriendsInfo$: Observable<FriendsInfo>; 
  tempFriendInfo: FriendsInfo = new FriendsInfo();

  constructor(
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.tempFriendsInfo$.subscribe(friendsInfo => {
      this.tempFriendInfo = friendsInfo;
    })
  }

  confirmInfo() {
    console.log("confirm info..");
    this.store.dispatch(new KeepFriendsInfo({data: this.tempFriendInfo})).subscribe(success => {
      this.router.navigate(['/confirm-to-share']);
    });
    
  }
}
