import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { GetFriendsInfo, ProfileState } from 'src/app/store/profile.actions';
import { ChildrenDetailComponent } from '../children-detail/children-detail.component';

@Component({
  selector: 'app-search-children',
  templateUrl: './search-children.component.html',
  styleUrls: ['./search-children.component.scss'],
})
export class SearchChildrenComponent implements OnInit {

  searchTxt: string = "";
  @Select(ProfileState.friendsInfoKey) friendsInfo$: Observable<Array<FriendsInfo>>; 
  friendInfo: Array<FriendsInfo> = [];

  constructor(
    private modalController: ModalController,
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.friendsInfo$.subscribe(friendsInfo => {
      this.friendInfo = friendsInfo;
      console.log("data", this.friendInfo);
    });

    this.store.dispatch(new GetFriendsInfo({data: this.searchTxt})).subscribe(data => {
      this.friendInfo = data;
    });
  }

  change_callback(ele) {
    console.log('called..', ele);
  }

  searchChildren() {
    this.store.dispatch(new GetFriendsInfo({data: this.searchTxt})).subscribe(data => {
      this.friendInfo = data;
    });
  }

  async detailPopup(child_id: number) {
    console.log('child_id', child_id);
    
    const childrenDetailPopup = await this.modalController.create({
      component: ChildrenDetailComponent,
      cssClass: "search-popup",
      componentProps: {
        child_id:child_id
      },
    });
    childrenDetailPopup.onDidDismiss().then((data) => {});
    return await childrenDetailPopup.present().then(() => {
      console.log("popup loaded");
    });
  }

}
