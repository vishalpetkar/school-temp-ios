import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FriendsInfo } from 'src/app/models/friend-model';
import { KeepFriendsInfo, ProfileState } from 'src/app/store/profile.actions';

@Component({
  selector: 'app-review-info',
  templateUrl: './review-info.component.html',
  styleUrls: ['./review-info.component.scss'],
})
export class ReviewInfoComponent implements OnInit {

  message: string = '';
  @Select(ProfileState.tempFriendInfoKey) tempFriendsInfo$: Observable<FriendsInfo>; 
  tempFriendInfo: FriendsInfo = new FriendsInfo();

  constructor(
    private alertController: AlertController,
    private router: Router,
    private store: Store) { }

  ngOnInit() {
    this.tempFriendsInfo$.subscribe(friendsInfo => {
      this.tempFriendInfo = friendsInfo;
      console.log('temp friend info ', this.tempFriendInfo);
    })
  }

  rewardOption() {
    console.log("ready to share and select reward option..", this.message);
    this.tempFriendInfo.message = this.message;
    console.log("this.tempFriendInfo", this.tempFriendInfo);

    if (this.message == '') {
      this.errorModal("Please enter message to continue.", 422);
      return false;
    }

    this.store.dispatch(new KeepFriendsInfo({data: this.tempFriendInfo})).subscribe(success => {
      this.router.navigate(['/reward-option']);
    });
  }


  // Error Modal
  async errorModal(msg, code) {
    const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Error',
        subHeader: '',
        message: msg,
        buttons: ['OK']
    });
    await alert.present();
  }

}

