import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { TermsComponent } from './pages/terms/terms.component';
import { ForgetPasswordComponent } from './pages/forget-password/forget-password.component';
import { SearchChildrenComponent } from './pages/search-children/search-children.component';
import { RewardOptionComponent } from './pages/reward-option/reward-option.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LandingPageComponent } from './pages/welcome/landing-page.component';
import { HowItWorksComponent } from './pages/how-it-works/how-it-works.component';
import { CampusAnonymousComponent } from './pages/friends-info/campus-anonymous.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { ConfirmAndShareComponent } from './pages/confirm-and-share/confirm-and-share.component';
import { LoginComponent } from './pages/login/login.component';
import { NotReadyToShareComponent } from './pages/not-ready-to-share/not-ready-to-share.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { SubmissionConfirmatoinComponent } from './pages/submission-confirmatoin/submission-confirmatoin.component';
import { VerifyOtpComponent } from './pages/verify-otp/verify-otp.component';
import { CreatePasswordComponent } from './pages/create-password/create-password.component';
import { ConfirmToShareComponent } from './pages/confirm-to-share/confirm-to-share.component';
import { ProfileState } from './store/profile.actions';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { HeaderInterceptor } from './interceptors/header.interceptor';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { ReviewInfoComponent } from './pages/review-info/review-info.component';
import { ChildrenDetailComponent } from './pages/children-detail/children-detail.component';
import { UpdatePasswordComponent } from './pages/update-password/update-password.component';

import { NgxRatingModule } from '@nikiphoros/ngx-rating';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HowItWorksComponent,
    CampusAnonymousComponent,
    ReviewInfoComponent,
    RewardOptionComponent,
    ChangePasswordComponent,
    ConfirmAndShareComponent,
    LoginComponent,
    NotReadyToShareComponent,
    ProfileComponent,
    RegisterComponent,
    SubmissionConfirmatoinComponent,
    VerifyOtpComponent,
    CreatePasswordComponent,
    ConfirmToShareComponent,
    SearchChildrenComponent,
    ChildrenDetailComponent,
    ForgetPasswordComponent,
    UpdatePasswordComponent,
    TermsComponent,
    PrivacyPolicyComponent,
    ContactUsComponent],
  entryComponents: [ChildrenDetailComponent],
  imports: [
    NgxRatingModule,
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forRoot([ProfileState], {
      developmentMode: false  //!environment.production
    })],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
