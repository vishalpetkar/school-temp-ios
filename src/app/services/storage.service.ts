/**
* Storage Services
* @author    ThemesBuckets <themebucketbd@gmail.com>
* @copyright Copyright (c) 2020
* @license   ThemesBuckets
*/

import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Profile } from '../models/school-model';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  async getObject(ITEMS_KEY) {
    const ret: any = await Storage.get({ key: ITEMS_KEY });
    const data = JSON.parse(ret.value);
    return data;
  }

  async setObject(profile, ITEMS_KEY) {
    await Storage.set({
      key: ITEMS_KEY,
      value: JSON.stringify(profile)
    });
  }

  async setProfile(profile: Profile, ITEMS_KEY) {
    await Storage.set({
      key: ITEMS_KEY,
      value: JSON.stringify(profile)
    });
  }

  async clearKey(ITEMS_KEY) {
    await Storage.remove({
      key: ITEMS_KEY
    });
  }

  async clear() {
    await Storage.clear();
  }
}
