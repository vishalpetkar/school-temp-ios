import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { Constant } from '../constant';
import { FriendsInfo, FriendsInfoData } from '../models/friend-model';
import { ChangePassword, LoginData, OtpData, Profile, ProfileData } from '../models/school-model';
import { StorageService } from './storage.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchoolServiceService {

  constructor(public storageService: StorageService,
      private http: HttpClient,
      private storage: StorageService,
      private store: Store,
      private injector: Injector) { }


  getFriendsInfo(searchTxt: string) {
    return this.http.get<FriendsInfoData>(environment.baseUrl + 'friendsinfo', {params:{txt:searchTxt}});
  }

  addFriendsInfo(obj: FriendsInfo) {
    return this.http.post<FriendsInfoData>(environment.baseUrl + 'friendsinfo', obj);
  }

  login(obj: Profile): Observable<LoginData> {
    return this.http.post<LoginData>(environment.baseUrl + 'login', obj);
  }

  registration(obj: Profile) {
    return this.http.post<ProfileData>(environment.baseUrl + 'register', obj);
  }

  sendOTP(profileObj: Profile) {
    console.log('project object = ', profileObj);
    return this.http.post<OtpData>(environment.baseUrl + 'send_otp', profileObj);
  }

  verifyOTP(profileObj: Profile) {
    return this.http.post<ProfileData>(environment.baseUrl + 'verify_otp', profileObj);
  }

  updateProfile(obj: Profile) {
    return this.http.post<ProfileData>(environment.baseUrl + 'update_profile', obj);
  }

  changePassword(obj: ChangePassword) {
    return this.http.post<ChangePassword>(environment.baseUrl + 'change_password', obj);
  }

  updatePassword(obj: ChangePassword) {
    return this.http.post<ChangePassword>(environment.baseUrl + 'update_password', obj);
  }

  logOut() {
    this.storage.clearKey(Constant.myLoginData);
    const store = this.injector.get<Store>(Store);
    return of([]);
  }

}


