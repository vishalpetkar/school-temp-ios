export class Profile {
    id: number;
    name: string;
    email: string;
    mobile: string;
    password: string;
    confirm_password: string;
    address: string;
    state: string;
    district: string;
    taluka: string;
    pincode: string;
    device_id: string;
    accesstoken: string;
    otp: string;
    action: string;
    type: string;

    constructor() {
        this.id = 0;
        this.name = '';
        this.email = '';
        this.mobile = '';
        this.password = '';
        this.confirm_password = '';
        this.address = '';
        this.state = '';
        this.district = '';
        this.taluka = '';
        this.pincode = '';
        this.device_id = '';
        this.accesstoken = '';
        this.otp = '';
        this.action = '';
        this.type = '';
    }
}

export class ProfileData {

    status: string;
    message: string;
    data: Profile;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Profile();
    }
}

class Login {

    user: Profile;
    access_token: string;
    token_type: string;
    expires_in: string;

    constructor() {
        this.user = new Profile();
        this.access_token = '';
        this.token_type = '';
        this.expires_in = '';
    }
}

export class LoginData {

    status: string;
    message: string;
    data: Login;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Login();
    }
}

export class ChangePassword {
    
    mobile: string;
    current_password: string;
    new_password: string;
    new_confirm_password: string;
    
    constructor() {

        this.mobile = '';
        this.current_password = '';
        this.new_password = '';
        this.new_confirm_password = '';
    }
}

export class OtpData {

    status: string;
    message: string;
    data: string;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}
