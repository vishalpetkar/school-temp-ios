
export class FriendsInfo {

    id: number;
    city: string;
    state: string;
    country: string;
    name: string;
    email: string;
    phone: string;
    school: string;
    grade: string;
    message: string;
    reward: string;
    lat: number;
    lng: number;

    constructor() {

        this.id = 0;
        this.city = '';
        this.state = '';
        this.country = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.school = '';
        this.grade = '';
        this.message = '';
        this.reward = '';
        this.lat = 0;
        this.lng = 0;
    }
}

export class FriendsInfoData {

    status: string;
    message: string;
    data: Array<FriendsInfo>;

    constructor() {
        this.status = '';
        this.message = '';
        this.data = [];
    }
}
