(self["webpackChunkannonymous"] = self["webpackChunkannonymous"] || []).push([["main"],{

/***/ 8255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 8255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _pages_search_children_search_children_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages/search-children/search-children.component */ 9129);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _pages_friends_info_campus_anonymous_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/friends-info/campus-anonymous.component */ 9056);
/* harmony import */ var _pages_confirm_and_share_confirm_and_share_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages/confirm-and-share/confirm-and-share.component */ 2280);
/* harmony import */ var _pages_how_it_works_how_it_works_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/how-it-works/how-it-works.component */ 3552);
/* harmony import */ var _pages_welcome_landing_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/welcome/landing-page.component */ 1808);
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/login/login.component */ 4902);
/* harmony import */ var _pages_not_ready_to_share_not_ready_to_share_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/not-ready-to-share/not-ready-to-share.component */ 2909);
/* harmony import */ var _pages_register_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/register/register.component */ 6698);
/* harmony import */ var _pages_submission_confirmatoin_submission_confirmatoin_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/submission-confirmatoin/submission-confirmatoin.component */ 8172);
/* harmony import */ var _pages_verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/verify-otp/verify-otp.component */ 3834);
/* harmony import */ var _pages_create_password_create_password_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/create-password/create-password.component */ 552);
/* harmony import */ var _pages_review_info_review_info_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/review-info/review-info.component */ 2541);
/* harmony import */ var _pages_reward_option_reward_option_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/reward-option/reward-option.component */ 3266);
/* harmony import */ var _pages_confirm_to_share_confirm_to_share_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/confirm-to-share/confirm-to-share.component */ 3926);
/* harmony import */ var _pages_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/forget-password/forget-password.component */ 8836);
/* harmony import */ var _pages_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/update-password/update-password.component */ 6599);
/* harmony import */ var _pages_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/change-password/change-password.component */ 7257);
/* harmony import */ var _pages_terms_terms_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/terms/terms.component */ 6830);
/* harmony import */ var _pages_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/contact-us/contact-us.component */ 9165);
/* harmony import */ var _pages_privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pages/privacy-policy/privacy-policy.component */ 3654);























const routes = [
    {
        path: '',
        redirectTo: 'landing',
        pathMatch: 'full'
    },
    {
        path: 'landing',
        component: _pages_welcome_landing_page_component__WEBPACK_IMPORTED_MODULE_4__.LandingPageComponent
    },
    {
        path: 'login',
        component: _pages_login_login_component__WEBPACK_IMPORTED_MODULE_5__.LoginComponent
    },
    {
        path: 'campus-aonoymous',
        component: _pages_friends_info_campus_anonymous_component__WEBPACK_IMPORTED_MODULE_1__.CampusAnonymousComponent
    },
    {
        path: 'how-it-works',
        component: _pages_how_it_works_how_it_works_component__WEBPACK_IMPORTED_MODULE_3__.HowItWorksComponent
    },
    {
        path: 'not-ready-to-share',
        component: _pages_not_ready_to_share_not_ready_to_share_component__WEBPACK_IMPORTED_MODULE_6__.NotReadyToShareComponent
    },
    {
        path: 'confirm-and-share',
        component: _pages_confirm_and_share_confirm_and_share_component__WEBPACK_IMPORTED_MODULE_2__.ConfirmAndShareComponent
    },
    {
        path: 'register',
        component: _pages_register_register_component__WEBPACK_IMPORTED_MODULE_7__.RegisterComponent
    },
    {
        path: 'forget-password',
        component: _pages_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_14__.ForgetPasswordComponent
    },
    {
        path: 'verification',
        component: _pages_verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_9__.VerifyOtpComponent
    },
    {
        path: 'update-password',
        component: _pages_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_15__.UpdatePasswordComponent
    },
    {
        path: 'create-password',
        component: _pages_create_password_create_password_component__WEBPACK_IMPORTED_MODULE_10__.CreatePasswordComponent
    },
    {
        path: 'review-info',
        component: _pages_review_info_review_info_component__WEBPACK_IMPORTED_MODULE_11__.ReviewInfoComponent
    },
    {
        path: 'reward-option',
        component: _pages_reward_option_reward_option_component__WEBPACK_IMPORTED_MODULE_12__.RewardOptionComponent
    },
    {
        path: 'confirm-to-share',
        component: _pages_confirm_to_share_confirm_to_share_component__WEBPACK_IMPORTED_MODULE_13__.ConfirmToShareComponent
    },
    {
        path: 'submission-confirmation',
        component: _pages_submission_confirmatoin_submission_confirmatoin_component__WEBPACK_IMPORTED_MODULE_8__.SubmissionConfirmatoinComponent
    },
    {
        path: 'search-children',
        component: _pages_search_children_search_children_component__WEBPACK_IMPORTED_MODULE_0__.SearchChildrenComponent
    },
    {
        path: 'change-password',
        component: _pages_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_16__.ChangePasswordComponent
    },
    {
        path: 'terms',
        component: _pages_terms_terms_component__WEBPACK_IMPORTED_MODULE_17__.TermsComponent
    },
    {
        path: 'privacy-policy',
        component: _pages_privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_19__.PrivacyPolicyComponent
    },
    {
        path: 'contact-us',
        component: _pages_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_18__.ContactUsComponent
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_20__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_21__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_22__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_22__.PreloadAllModules })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_22__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 1106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 19);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var _models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./models/school-model */ 5039);
/* harmony import */ var _store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store/profile.actions */ 3069);









let AppComponent = class AppComponent {
    constructor(store, router, menuController, modalController, actionSheetCtrl, popoverCtrl, toastController, platform) {
        this.store = store;
        this.router = router;
        this.menuController = menuController;
        this.modalController = modalController;
        this.actionSheetCtrl = actionSheetCtrl;
        this.popoverCtrl = popoverCtrl;
        this.toastController = toastController;
        this.platform = platform;
        this.lastTimeBackPress = 0;
        this.timePeriodToExit = 2000;
        this.profileData = new _models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
        this.appPages = [
            { title: 'Logout', url: '/landing', icon: 'home' }
        ];
        this.backButtonEvent();
    }
    ngOnInit() {
        this.profile$.subscribe(profile => {
            this.profileData = profile;
            console.log('=profile=', this.profileData);
        });
    }
    backButtonEvent() {
        this.platform.backButton.subscribeWithPriority(0, () => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            // close action sheet
            try {
                const element = yield this.actionSheetCtrl.getTop();
                if (element) {
                    console.log('Action sheet');
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
            }
            // close popover
            try {
                const element = yield this.popoverCtrl.getTop();
                if (element) {
                    console.log('Popover controller');
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
            }
            // close modal
            try {
                const element = yield this.modalController.getTop();
                if (element) {
                    console.log('Modal');
                    element.dismiss();
                    return;
                }
            }
            catch (error) {
                console.log(error);
            }
            // close side menu
            try {
                const element = yield this.menuController.getOpen();
                console.log(element);
                if (element !== undefined) {
                    console.log('Side Menu');
                    this.menuController.close();
                    return;
                }
            }
            catch (error) {
            }
            try {
                this.routerOutlets.forEach((outlet) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                    console.log('back => ', outlet, this.router.routerState.snapshot.url);
                    if (!['/driver/home', '/admin/home', '/login'].some(x => x == this.router.routerState.snapshot.url)) {
                        // await this.location.back();
                        console.log('done back');
                    }
                    else {
                        if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
                            this.lastTimeBackPress = new Date().getTime();
                            this.toster("press again to exit from app");
                        }
                        else {
                            navigator['app'].exitApp();
                        }
                    }
                    return false;
                }));
            }
            catch (error) {
            }
        }));
    }
    toster($msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: $msg,
                duration: 1500
            });
            toast.present();
        });
    }
    signOut() {
        this.store.dispatch(new _store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ClearProfile()).subscribe(suc => {
            this.menuController.toggle();
            this.router.navigate(['/login']);
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.MenuController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ActionSheetController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.PopoverController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.Platform }
];
AppComponent.propDecorators = {
    routerOutlets: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChildren, args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonRouterOutlet,] }]
};
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.profileKey)
], AppComponent.prototype, "profile$", void 0);
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _pages_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages/contact-us/contact-us.component */ 9165);
/* harmony import */ var _pages_privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/privacy-policy/privacy-policy.component */ 3654);
/* harmony import */ var _pages_terms_terms_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages/terms/terms.component */ 6830);
/* harmony import */ var _pages_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/forget-password/forget-password.component */ 8836);
/* harmony import */ var _pages_search_children_search_children_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/search-children/search-children.component */ 9129);
/* harmony import */ var _pages_reward_option_reward_option_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/reward-option/reward-option.component */ 3266);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _pages_welcome_landing_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/welcome/landing-page.component */ 1808);
/* harmony import */ var _pages_how_it_works_how_it_works_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/how-it-works/how-it-works.component */ 3552);
/* harmony import */ var _pages_friends_info_campus_anonymous_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/friends-info/campus-anonymous.component */ 9056);
/* harmony import */ var _pages_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/change-password/change-password.component */ 7257);
/* harmony import */ var _pages_confirm_and_share_confirm_and_share_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/confirm-and-share/confirm-and-share.component */ 2280);
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/login/login.component */ 4902);
/* harmony import */ var _pages_not_ready_to_share_not_ready_to_share_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/not-ready-to-share/not-ready-to-share.component */ 2909);
/* harmony import */ var _pages_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/profile/profile.component */ 8220);
/* harmony import */ var _pages_register_register_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/register/register.component */ 6698);
/* harmony import */ var _pages_submission_confirmatoin_submission_confirmatoin_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/submission-confirmatoin/submission-confirmatoin.component */ 8172);
/* harmony import */ var _pages_verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/verify-otp/verify-otp.component */ 3834);
/* harmony import */ var _pages_create_password_create_password_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pages/create-password/create-password.component */ 552);
/* harmony import */ var _pages_confirm_to_share_confirm_to_share_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./pages/confirm-to-share/confirm-to-share.component */ 3926);
/* harmony import */ var _store_profile_actions__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./store/profile.actions */ 3069);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var _interceptors_header_interceptor__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./interceptors/header.interceptor */ 1150);
/* harmony import */ var _interceptors_http_error_interceptor__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./interceptors/http-error.interceptor */ 6276);
/* harmony import */ var _pages_review_info_review_info_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pages/review-info/review-info.component */ 2541);
/* harmony import */ var _pages_children_detail_children_detail_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./pages/children-detail/children-detail.component */ 4815);
/* harmony import */ var _pages_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./pages/update-password/update-password.component */ 6599);
/* harmony import */ var _nikiphoros_ngx_rating__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @nikiphoros/ngx-rating */ 789);




































let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_28__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_29__.NgModule)({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_6__.AppComponent,
            _pages_welcome_landing_page_component__WEBPACK_IMPORTED_MODULE_8__.LandingPageComponent,
            _pages_how_it_works_how_it_works_component__WEBPACK_IMPORTED_MODULE_9__.HowItWorksComponent,
            _pages_friends_info_campus_anonymous_component__WEBPACK_IMPORTED_MODULE_10__.CampusAnonymousComponent,
            _pages_review_info_review_info_component__WEBPACK_IMPORTED_MODULE_25__.ReviewInfoComponent,
            _pages_reward_option_reward_option_component__WEBPACK_IMPORTED_MODULE_5__.RewardOptionComponent,
            _pages_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_11__.ChangePasswordComponent,
            _pages_confirm_and_share_confirm_and_share_component__WEBPACK_IMPORTED_MODULE_12__.ConfirmAndShareComponent,
            _pages_login_login_component__WEBPACK_IMPORTED_MODULE_13__.LoginComponent,
            _pages_not_ready_to_share_not_ready_to_share_component__WEBPACK_IMPORTED_MODULE_14__.NotReadyToShareComponent,
            _pages_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__.ProfileComponent,
            _pages_register_register_component__WEBPACK_IMPORTED_MODULE_16__.RegisterComponent,
            _pages_submission_confirmatoin_submission_confirmatoin_component__WEBPACK_IMPORTED_MODULE_17__.SubmissionConfirmatoinComponent,
            _pages_verify_otp_verify_otp_component__WEBPACK_IMPORTED_MODULE_18__.VerifyOtpComponent,
            _pages_create_password_create_password_component__WEBPACK_IMPORTED_MODULE_19__.CreatePasswordComponent,
            _pages_confirm_to_share_confirm_to_share_component__WEBPACK_IMPORTED_MODULE_20__.ConfirmToShareComponent,
            _pages_search_children_search_children_component__WEBPACK_IMPORTED_MODULE_4__.SearchChildrenComponent,
            _pages_children_detail_children_detail_component__WEBPACK_IMPORTED_MODULE_26__.ChildrenDetailComponent,
            _pages_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_3__.ForgetPasswordComponent,
            _pages_update_password_update_password_component__WEBPACK_IMPORTED_MODULE_27__.UpdatePasswordComponent,
            _pages_terms_terms_component__WEBPACK_IMPORTED_MODULE_2__.TermsComponent,
            _pages_privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_1__.PrivacyPolicyComponent,
            _pages_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_0__.ContactUsComponent
        ],
        entryComponents: [_pages_children_detail_children_detail_component__WEBPACK_IMPORTED_MODULE_26__.ChildrenDetailComponent],
        imports: [
            _nikiphoros_ngx_rating__WEBPACK_IMPORTED_MODULE_30__.NgxRatingModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_31__.HttpClientModule,
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_32__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_33__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_7__.AppRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_34__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_34__.ReactiveFormsModule,
            _ngxs_store__WEBPACK_IMPORTED_MODULE_22__.NgxsModule.forRoot([_store_profile_actions__WEBPACK_IMPORTED_MODULE_21__.ProfileState], {
                developmentMode: false //!environment.production
            })
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_35__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_33__.IonicRouteStrategy },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_31__.HTTP_INTERCEPTORS, useClass: _interceptors_header_interceptor__WEBPACK_IMPORTED_MODULE_23__.HeaderInterceptor, multi: true },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_31__.HTTP_INTERCEPTORS, useClass: _interceptors_http_error_interceptor__WEBPACK_IMPORTED_MODULE_24__.HttpErrorInterceptor, multi: true }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 2831:
/*!*****************************!*\
  !*** ./src/app/constant.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Constant": () => (/* binding */ Constant)
/* harmony export */ });
const Constant = {
    myAction: 'my-action',
    myLoginData: 'my-login-data',
    currencyIcon: '₹'
};


/***/ }),

/***/ 1150:
/*!****************************************************!*\
  !*** ./src/app/interceptors/header.interceptor.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderInterceptor": () => (/* binding */ HeaderInterceptor)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _store_profile_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../store/profile.actions */ 3069);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngxs/store */ 3394);




let HeaderInterceptor = class HeaderInterceptor {
    constructor() {
        this.access_token = '';
    }
    intercept(request, next) {
        this.profile$.subscribe(token => {
            this.access_token = token;
        });
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.access_token}`
            }
        });
        return next.handle(request);
    }
};
HeaderInterceptor.ctorParameters = () => [];
(0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_1__.Select)(_store_profile_actions__WEBPACK_IMPORTED_MODULE_0__.ProfileState.accesstokenKey)
], HeaderInterceptor.prototype, "profile$", void 0);
HeaderInterceptor = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)()
], HeaderInterceptor);



/***/ }),

/***/ 6276:
/*!********************************************************!*\
  !*** ./src/app/interceptors/http-error.interceptor.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpErrorInterceptor": () => (/* binding */ HttpErrorInterceptor)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 205);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 8307);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _store_profile_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../store/profile.actions */ 3069);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);









let HttpErrorInterceptor = class HttpErrorInterceptor {
    constructor(loadingController, toastController, router, store, alertController) {
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.router = router;
        this.store = store;
        this.alertController = alertController;
        this.loaderForGet = [
            'admin_dashboard',
            'vehicle_category'
        ];
    }
    intercept(request, next) {
        // Start Loader
        if (request.method !== "GET") {
            this.presentLoading();
        }
        else {
            console.log('request', request);
            if (this.loaderForGet.some(v => request.url.includes(v))) {
                this.presentLoading();
            }
        }
        // ./ End Loader /.
        return next.handle(request)
            .pipe(
        // Handle Success Message
        (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.tap)(evt => {
            if (this.loader != undefined) {
                this.loader.dismiss();
            }
            if (evt instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpResponse) {
                if (evt.body && (evt.status == 200 || evt.status == 201) && evt.body.message != null) {
                    this.successToast(evt.body.message);
                }
            }
        }), 
        // ./ Handle Success Message /.
        // Handle Error Message
        (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.catchError)((error) => {
            if (this.loader != undefined) {
                this.loader.dismiss();
            }
            let errorMsg = '';
            let errorCode = '';
            let code = 0;
            if (error.error instanceof ErrorEvent) {
                errorMsg = `Error: ${error.error.message}`;
            }
            else {
                errorMsg = `Message: ${error.error.message}`;
                errorCode = `Code: ${error.status}`;
                code = error.status;
            }
            this.errorModal(errorMsg, errorCode);
            if (code === 401) {
                this.store.dispatch(new _store_profile_actions__WEBPACK_IMPORTED_MODULE_0__.ClearProfile).subscribe(profile => {
                    this.router.navigate(['/login']);
                });
            }
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_5__.throwError)(errorMsg);
        })
        // ./ Handle Error Message /.
        );
    }
    // Error Modal
    errorModal(msg, code) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                subHeader: '',
                message: msg,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    // Success Toast
    successToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (msg != "") {
                const toast = yield this.toastController.create({
                    message: msg,
                    duration: 3000,
                    position: 'middle'
                });
                toast.present();
            }
        });
    }
    // Loader
    presentLoading() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            console.log('this.loader', this.loader);
            this.loader = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 3000
            });
            yield this.loader.present();
        });
    }
};
HttpErrorInterceptor.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_1__.Store },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.AlertController }
];
HttpErrorInterceptor = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)()
], HttpErrorInterceptor);



/***/ }),

/***/ 2692:
/*!****************************************!*\
  !*** ./src/app/models/friend-model.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FriendsInfo": () => (/* binding */ FriendsInfo),
/* harmony export */   "FriendsInfoData": () => (/* binding */ FriendsInfoData)
/* harmony export */ });
class FriendsInfo {
    constructor() {
        this.id = 0;
        this.city = '';
        this.state = '';
        this.country = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.school = '';
        this.grade = '';
        this.message = '';
        this.reward = '';
        this.lat = 0;
        this.lng = 0;
    }
}
class FriendsInfoData {
    constructor() {
        this.status = '';
        this.message = '';
        this.data = [];
    }
}


/***/ }),

/***/ 5039:
/*!****************************************!*\
  !*** ./src/app/models/school-model.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Profile": () => (/* binding */ Profile),
/* harmony export */   "ProfileData": () => (/* binding */ ProfileData),
/* harmony export */   "LoginData": () => (/* binding */ LoginData),
/* harmony export */   "ChangePassword": () => (/* binding */ ChangePassword),
/* harmony export */   "OtpData": () => (/* binding */ OtpData)
/* harmony export */ });
class Profile {
    constructor() {
        this.id = 0;
        this.name = '';
        this.email = '';
        this.mobile = '';
        this.password = '';
        this.confirm_password = '';
        this.address = '';
        this.state = '';
        this.district = '';
        this.taluka = '';
        this.pincode = '';
        this.device_id = '';
        this.accesstoken = '';
        this.otp = '';
        this.action = '';
        this.type = '';
    }
}
class ProfileData {
    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Profile();
    }
}
class Login {
    constructor() {
        this.user = new Profile();
        this.access_token = '';
        this.token_type = '';
        this.expires_in = '';
    }
}
class LoginData {
    constructor() {
        this.status = '';
        this.message = '';
        this.data = new Login();
    }
}
class ChangePassword {
    constructor() {
        this.mobile = '';
        this.current_password = '';
        this.new_password = '';
        this.new_confirm_password = '';
    }
}
class OtpData {
    constructor() {
        this.status = '';
        this.message = '';
        this.data = '';
    }
}


/***/ }),

/***/ 7257:
/*!********************************************************************!*\
  !*** ./src/app/pages/change-password/change-password.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangePasswordComponent": () => (/* binding */ ChangePasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_change_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./change-password.component.html */ 2957);
/* harmony import */ var _change_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-password.component.scss */ 7752);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_services_school_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/school-service.service */ 3273);








let ChangePasswordComponent = class ChangePasswordComponent {
    constructor(router, formBuilder, authService, loadingController) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.loadingController = loadingController;
        this.ChangeForm = formBuilder.group({
            current_password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            new_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required],
            new_confirm_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]
        });
    }
    ngOnInit() { }
    goToLogin() {
        this.router.navigate(['/login']);
    }
    change() {
        console.log(this.ChangeForm.value);
        this.authService.changePassword(this.ChangeForm.value).subscribe(success => {
            console.log(success);
            this.ChangeForm.reset();
            this.router.navigate(['/home']);
        }, err => {
        });
    }
};
ChangePasswordComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: src_app_services_school_service_service__WEBPACK_IMPORTED_MODULE_2__.SchoolServiceService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
ChangePasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-change-password',
        template: _raw_loader_change_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_change_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ChangePasswordComponent);



/***/ }),

/***/ 4815:
/*!********************************************************************!*\
  !*** ./src/app/pages/children-detail/children-detail.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChildrenDetailComponent": () => (/* binding */ ChildrenDetailComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_children_detail_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./children-detail.component.html */ 7343);
/* harmony import */ var _children_detail_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./children-detail.component.scss */ 5157);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let ChildrenDetailComponent = class ChildrenDetailComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.child_id = 0;
        this.friendInfo = [];
        this.friendInfoDetail = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
        console.log("child_id = " + this.child_id);
        this.friendsInfo$.subscribe(friendsInfo => {
            this.friendInfo = friendsInfo;
            console.log("data", this.friendInfo);
            const idx = this.friendInfo.findIndex(obj => obj.id === this.child_id);
            console.log('record', this.friendInfo[idx]);
            this.friendInfoDetail = this.friendInfo[idx];
        });
    }
    dismiss() {
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    }
};
ChildrenDetailComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.friendsInfoKey)
], ChildrenDetailComponent.prototype, "friendsInfo$", void 0);
ChildrenDetailComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-children-detail',
        template: _raw_loader_children_detail_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_children_detail_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ChildrenDetailComponent);



/***/ }),

/***/ 2280:
/*!************************************************************************!*\
  !*** ./src/app/pages/confirm-and-share/confirm-and-share.component.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConfirmAndShareComponent": () => (/* binding */ ConfirmAndShareComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_confirm_and_share_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./confirm-and-share.component.html */ 5238);
/* harmony import */ var _confirm_and_share_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./confirm-and-share.component.scss */ 3174);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let ConfirmAndShareComponent = class ConfirmAndShareComponent {
    constructor() { }
    ngOnInit() { }
};
ConfirmAndShareComponent.ctorParameters = () => [];
ConfirmAndShareComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-confirm-and-share',
        template: _raw_loader_confirm_and_share_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_confirm_and_share_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ConfirmAndShareComponent);



/***/ }),

/***/ 3926:
/*!**********************************************************************!*\
  !*** ./src/app/pages/confirm-to-share/confirm-to-share.component.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConfirmToShareComponent": () => (/* binding */ ConfirmToShareComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_confirm_to_share_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./confirm-to-share.component.html */ 2887);
/* harmony import */ var _confirm_to_share_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./confirm-to-share.component.scss */ 4906);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ 9315);
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ 7152);
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ 6030);











let ConfirmToShareComponent = class ConfirmToShareComponent {
    constructor(locationAccuracy, androidPermissions, geolocation, router, store) {
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.geolocation = geolocation;
        this.router = router;
        this.store = store;
        this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
        this.tempFriendsInfo$.subscribe(friendsInfo => {
            this.tempFriendInfo = friendsInfo;
        });
        this.checkGPSPermission();
    }
    submit() {
        console.log("submission confirmation");
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.AddFriendsInfo({ data: this.tempFriendInfo })).subscribe(success => {
            this.router.navigate(['/submission-confirmation']);
        });
    }
    requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest) => {
            if (canRequest) {
            }
            else {
                //Show 'GPS Permission Request' dialogue
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(() => {
                    // call method to turn on GPS
                    this.askToTurnOnGPS();
                }, error => {
                    //Show alert if user click on 'No Thanks'
                    // alert('requestPermission Error requesting location permissions ' + error)
                });
            }
        });
    }
    //Check if application having GPS access permission  
    checkGPSPermission() {
        // return ;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(result => {
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                this.askToTurnOnGPS();
            }
            else {
                //If not having permission ask for permission
                this.requestGPSPermission();
            }
        }, err => {
            alert(err);
        });
    }
    askToTurnOnGPS() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            // When GPS Turned ON call method to get Accurate location coordinates
            this.getLocationAndLoadData();
        }, error => {
            // alert('Error requesting location permissions ' + JSON.stringify(error))
            // alert('Please allow access the location service, to get shops near by you.');
            this.checkGPSPermission();
        });
    }
    getLocationAndLoadData() {
        this.geolocation.getCurrentPosition({ maximumAge: 1000, timeout: 5000, enableHighAccuracy: true }).then((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            console.log('location', resp, resp.coords.longitude, resp.coords.latitude);
            this.tempFriendInfo.lat = resp.coords.latitude;
            this.tempFriendInfo.lng = resp.coords.longitude;
        }));
    }
};
ConfirmToShareComponent.ctorParameters = () => [
    { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__.LocationAccuracy },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_5__.AndroidPermissions },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__.Geolocation },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempFriendInfoKey)
], ConfirmToShareComponent.prototype, "tempFriendsInfo$", void 0);
ConfirmToShareComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-confirm-to-share',
        template: _raw_loader_confirm_to_share_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        providers: [_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__.Geolocation, _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__.LocationAccuracy, _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_5__.AndroidPermissions],
        styles: [_confirm_to_share_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ConfirmToShareComponent);



/***/ }),

/***/ 9165:
/*!**********************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.component.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ContactUsComponent": () => (/* binding */ ContactUsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_contact_us_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./contact-us.component.html */ 2470);
/* harmony import */ var _contact_us_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./contact-us.component.scss */ 385);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let ContactUsComponent = class ContactUsComponent {
    constructor() { }
    ngOnInit() { }
};
ContactUsComponent.ctorParameters = () => [];
ContactUsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-contact-us',
        template: _raw_loader_contact_us_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_contact_us_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ContactUsComponent);



/***/ }),

/***/ 552:
/*!********************************************************************!*\
  !*** ./src/app/pages/create-password/create-password.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CreatePasswordComponent": () => (/* binding */ CreatePasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_create_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./create-password.component.html */ 4901);
/* harmony import */ var _create_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create-password.component.scss */ 2830);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let CreatePasswordComponent = class CreatePasswordComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.tempUserData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
    }
    ngOnInit() {
        this.tempprofile$.subscribe(data => {
            this.tempUserData = data;
            console.log("Temp User Data", this.tempUserData);
        });
    }
    submit() {
        console.log("create password");
        this.tempUserData.password = this.password;
        this.tempUserData.confirm_password = this.confirm_password;
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.AddProfile({ data: this.tempUserData })).subscribe(success => {
            console.log("temp profileData", this.tempUserData);
            this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.GetProfile({ data: this.tempUserData })).subscribe(success => {
                if (this.tempUserData.type == 'parent') {
                    this.router.navigate(['/search-children']);
                }
                else {
                    this.router.navigate(['/campus-aonoymous']);
                }
            });
        });
    }
};
CreatePasswordComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempUserKey)
], CreatePasswordComponent.prototype, "tempprofile$", void 0);
CreatePasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-create-password',
        template: _raw_loader_create_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_create_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], CreatePasswordComponent);



/***/ }),

/***/ 8836:
/*!********************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgetPasswordComponent": () => (/* binding */ ForgetPasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_forget_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./forget-password.component.html */ 226);
/* harmony import */ var _forget_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forget-password.component.scss */ 5047);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let ForgetPasswordComponent = class ForgetPasswordComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.phone = '';
        this.tempProfile = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
    }
    ngOnInit() { }
    sendOtp() {
        this.tempProfile.mobile = this.phone;
        this.tempProfile.action = 'forget';
        console.log('temp profile', this.tempProfile);
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.SendOTP({ data: this.tempProfile })).subscribe(success => {
            this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.TempProfile({ data: this.tempProfile })).subscribe(success => {
                this.router.navigate(['/verification']);
            });
        });
    }
};
ForgetPasswordComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
ForgetPasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-forget-password',
        template: _raw_loader_forget_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_forget_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ForgetPasswordComponent);



/***/ }),

/***/ 9056:
/*!******************************************************************!*\
  !*** ./src/app/pages/friends-info/campus-anonymous.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CampusAnonymousComponent": () => (/* binding */ CampusAnonymousComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_campus_anonymous_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./campus-anonymous.component.html */ 1278);
/* harmony import */ var _campus_anonymous_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./campus-anonymous.component.scss */ 6186);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage.service */ 1188);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);










let CampusAnonymousComponent = class CampusAnonymousComponent {
    constructor(alertController, router, store, storage) {
        this.alertController = alertController;
        this.router = router;
        this.store = store;
        this.storage = storage;
        this.city = '';
        this.state = '';
        this.country = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.school = '';
        this.grade = '';
        this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.storage.getObject('friends_info').then((friendsInfo) => {
            console.log('friendsInfo', friendsInfo);
            if (friendsInfo == null) {
                console.log('log 1 ', friendsInfo);
                this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
                this.name = '';
                this.email = '';
                this.phone = '';
                this.school = '';
                this.grade = '';
                this.city = '';
                this.state = '';
                this.country = '';
            }
            else {
                console.log('log 2 ', friendsInfo);
                this.tempFriendInfo = friendsInfo;
            }
        });
    }
    getFromDraft() {
        this.name = this.tempFriendInfo.name;
        this.email = this.tempFriendInfo.email;
        this.phone = this.tempFriendInfo.phone;
        this.school = this.tempFriendInfo.school;
        this.grade = this.tempFriendInfo.grade;
        this.city = this.tempFriendInfo.city;
        this.state = this.tempFriendInfo.state;
        this.country = this.tempFriendInfo.country;
    }
    share(type = 'ready') {
        if (this.name == '') {
            this.errorModal("Please enter your friend name.", 422);
            return false;
        }
        if (this.phone == '') {
            this.errorModal("Please enter your friend phone number.", 422);
            return false;
        }
        if (this.email == '') {
            this.errorModal("Please enter your friend email.", 422);
            return false;
        }
        if (this.school == '') {
            this.errorModal("Please enter school name.", 422);
            return false;
        }
        if (this.grade == '') {
            this.errorModal("Please enter grade.", 422);
            return false;
        }
        console.log("ready to share info");
        const friendsInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
        friendsInfo.city = this.city;
        friendsInfo.state = this.state;
        friendsInfo.country = this.country;
        friendsInfo.name = this.name;
        friendsInfo.email = this.email;
        friendsInfo.phone = this.phone;
        friendsInfo.school = this.school;
        friendsInfo.grade = this.grade;
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__.KeepFriendsInfo({ data: friendsInfo })).subscribe(success => {
            this.storage.setObject((friendsInfo), 'friends_info');
            this.name = '';
            this.email = '';
            this.phone = '';
            this.school = '';
            this.grade = '';
            this.city = '';
            this.state = '';
            this.country = '';
            if (type == 'ready') {
                this.router.navigate(['/review-info']);
            }
            else {
                this.router.navigate(['/not-ready-to-share']);
            }
        });
    }
    // Error Modal
    errorModal(msg, code) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                subHeader: '',
                message: msg,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
CampusAnonymousComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.AlertController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__.StorageService }
];
CampusAnonymousComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-campus-anonymous',
        template: _raw_loader_campus_anonymous_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        providers: [Storage],
        styles: [_campus_anonymous_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], CampusAnonymousComponent);



/***/ }),

/***/ 3552:
/*!**************************************************************!*\
  !*** ./src/app/pages/how-it-works/how-it-works.component.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HowItWorksComponent": () => (/* binding */ HowItWorksComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_how_it_works_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./how-it-works.component.html */ 6429);
/* harmony import */ var _how_it_works_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./how-it-works.component.scss */ 9073);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let HowItWorksComponent = class HowItWorksComponent {
    constructor() { }
    ngOnInit() { }
};
HowItWorksComponent.ctorParameters = () => [];
HowItWorksComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-how-it-works',
        template: _raw_loader_how_it_works_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_how_it_works_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HowItWorksComponent);



/***/ }),

/***/ 4902:
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginComponent": () => (/* binding */ LoginComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.component.html */ 911);
/* harmony import */ var _login_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component.scss */ 5100);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let LoginComponent = class LoginComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.tempUserData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
        this.email = '';
        this.password = '';
    }
    ngOnInit() { }
    login() {
        this.tempUserData.email = this.email;
        this.tempUserData.password = this.password;
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.GetProfile({ data: this.tempUserData })).subscribe(success => {
            this.email = '';
            this.password = '';
            this.router.navigate(['/campus-aonoymous']);
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
LoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginComponent);



/***/ }),

/***/ 2909:
/*!**************************************************************************!*\
  !*** ./src/app/pages/not-ready-to-share/not-ready-to-share.component.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotReadyToShareComponent": () => (/* binding */ NotReadyToShareComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_not_ready_to_share_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./not-ready-to-share.component.html */ 9814);
/* harmony import */ var _not_ready_to_share_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./not-ready-to-share.component.scss */ 3871);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);







let NotReadyToShareComponent = class NotReadyToShareComponent {
    constructor() {
        this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
        this.tempFriendsInfo$.subscribe(friendsInfo => {
            console.log('friendsInfo ', friendsInfo);
            this.tempFriendInfo = friendsInfo;
            console.log('temp friend info ', this.tempFriendInfo);
        });
    }
};
NotReadyToShareComponent.ctorParameters = () => [];
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempFriendInfoKey)
], NotReadyToShareComponent.prototype, "tempFriendsInfo$", void 0);
NotReadyToShareComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-not-ready-to-share',
        template: _raw_loader_not_ready_to_share_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_not_ready_to_share_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], NotReadyToShareComponent);



/***/ }),

/***/ 3654:
/*!******************************************************************!*\
  !*** ./src/app/pages/privacy-policy/privacy-policy.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PrivacyPolicyComponent": () => (/* binding */ PrivacyPolicyComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_privacy_policy_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./privacy-policy.component.html */ 2209);
/* harmony import */ var _privacy_policy_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./privacy-policy.component.scss */ 7902);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let PrivacyPolicyComponent = class PrivacyPolicyComponent {
    constructor() { }
    ngOnInit() { }
};
PrivacyPolicyComponent.ctorParameters = () => [];
PrivacyPolicyComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-privacy-policy',
        template: _raw_loader_privacy_policy_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_privacy_policy_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PrivacyPolicyComponent);



/***/ }),

/***/ 8220:
/*!****************************************************!*\
  !*** ./src/app/pages/profile/profile.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfileComponent": () => (/* binding */ ProfileComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./profile.component.html */ 9524);
/* harmony import */ var _profile_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.component.scss */ 8707);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let ProfileComponent = class ProfileComponent {
    constructor() { }
    ngOnInit() { }
};
ProfileComponent.ctorParameters = () => [];
ProfileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-profile',
        template: _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_profile_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ProfileComponent);



/***/ }),

/***/ 6698:
/*!******************************************************!*\
  !*** ./src/app/pages/register/register.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterComponent": () => (/* binding */ RegisterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_register_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./register.component.html */ 4073);
/* harmony import */ var _register_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.component.scss */ 8214);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let RegisterComponent = class RegisterComponent {
    constructor() { }
    ngOnInit() { }
};
RegisterComponent.ctorParameters = () => [];
RegisterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-register',
        template: _raw_loader_register_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_register_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RegisterComponent);



/***/ }),

/***/ 2541:
/*!************************************************************!*\
  !*** ./src/app/pages/review-info/review-info.component.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReviewInfoComponent": () => (/* binding */ ReviewInfoComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_review_info_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./review-info.component.html */ 3330);
/* harmony import */ var _review_info_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./review-info.component.scss */ 1331);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);









let ReviewInfoComponent = class ReviewInfoComponent {
    constructor(alertController, router, store) {
        this.alertController = alertController;
        this.router = router;
        this.store = store;
        this.message = '';
        this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
        this.tempFriendsInfo$.subscribe(friendsInfo => {
            this.tempFriendInfo = friendsInfo;
            console.log('temp friend info ', this.tempFriendInfo);
        });
    }
    rewardOption() {
        console.log("ready to share and select reward option..", this.message);
        this.tempFriendInfo.message = this.message;
        console.log("this.tempFriendInfo", this.tempFriendInfo);
        if (this.message == '') {
            this.errorModal("Please enter message to continue.", 422);
            return false;
        }
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.KeepFriendsInfo({ data: this.tempFriendInfo })).subscribe(success => {
            this.router.navigate(['/reward-option']);
        });
    }
    // Error Modal
    errorModal(msg, code) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                subHeader: '',
                message: msg,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
ReviewInfoComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.AlertController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempFriendInfoKey)
], ReviewInfoComponent.prototype, "tempFriendsInfo$", void 0);
ReviewInfoComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-review-info',
        template: _raw_loader_review_info_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_review_info_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ReviewInfoComponent);



/***/ }),

/***/ 3266:
/*!****************************************************************!*\
  !*** ./src/app/pages/reward-option/reward-option.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RewardOptionComponent": () => (/* binding */ RewardOptionComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_reward_option_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./reward-option.component.html */ 4807);
/* harmony import */ var _reward_option_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reward-option.component.scss */ 869);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let RewardOptionComponent = class RewardOptionComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.reward = '';
        this.tempFriendInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
    }
    ngOnInit() {
        this.tempFriendsInfo$.subscribe(friendsInfo => {
            this.tempFriendInfo = friendsInfo;
        });
    }
    confirmInfo() {
        console.log("confirm info..");
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.KeepFriendsInfo({ data: this.tempFriendInfo })).subscribe(success => {
            this.router.navigate(['/confirm-to-share']);
        });
    }
};
RewardOptionComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempFriendInfoKey)
], RewardOptionComponent.prototype, "tempFriendsInfo$", void 0);
RewardOptionComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-reward-option',
        template: _raw_loader_reward_option_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_reward_option_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RewardOptionComponent);



/***/ }),

/***/ 9129:
/*!********************************************************************!*\
  !*** ./src/app/pages/search-children/search-children.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SearchChildrenComponent": () => (/* binding */ SearchChildrenComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_search_children_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./search-children.component.html */ 9378);
/* harmony import */ var _search_children_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-children.component.scss */ 1311);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);
/* harmony import */ var _children_detail_children_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../children-detail/children-detail.component */ 4815);









let SearchChildrenComponent = class SearchChildrenComponent {
    constructor(modalController, router, store) {
        this.modalController = modalController;
        this.router = router;
        this.store = store;
        this.searchTxt = "";
        this.friendInfo = [];
    }
    ngOnInit() {
        this.friendsInfo$.subscribe(friendsInfo => {
            this.friendInfo = friendsInfo;
            console.log("data", this.friendInfo);
        });
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_3__.GetFriendsInfo({ data: this.searchTxt })).subscribe(data => {
            this.friendInfo = data;
        });
    }
    change_callback(ele) {
        console.log('called..', ele);
    }
    searchChildren() {
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_3__.GetFriendsInfo({ data: this.searchTxt })).subscribe(data => {
            this.friendInfo = data;
        });
    }
    detailPopup(child_id) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log('child_id', child_id);
            const childrenDetailPopup = yield this.modalController.create({
                component: _children_detail_children_detail_component__WEBPACK_IMPORTED_MODULE_4__.ChildrenDetailComponent,
                cssClass: "search-popup",
                componentProps: {
                    child_id: child_id
                },
            });
            childrenDetailPopup.onDidDismiss().then((data) => { });
            return yield childrenDetailPopup.present().then(() => {
                console.log("popup loaded");
            });
        });
    }
};
SearchChildrenComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_3__.ProfileState.friendsInfoKey)
], SearchChildrenComponent.prototype, "friendsInfo$", void 0);
SearchChildrenComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-search-children',
        template: _raw_loader_search_children_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_search_children_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SearchChildrenComponent);



/***/ }),

/***/ 8172:
/*!************************************************************************************!*\
  !*** ./src/app/pages/submission-confirmatoin/submission-confirmatoin.component.ts ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SubmissionConfirmatoinComponent": () => (/* binding */ SubmissionConfirmatoinComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_submission_confirmatoin_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./submission-confirmatoin.component.html */ 8232);
/* harmony import */ var _submission_confirmatoin_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./submission-confirmatoin.component.scss */ 6789);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/friend-model */ 2692);
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage.service */ 1188);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ 4276);









let SubmissionConfirmatoinComponent = class SubmissionConfirmatoinComponent {
    constructor(storage, store, socialSharing) {
        this.storage = storage;
        this.store = store;
        this.socialSharing = socialSharing;
    }
    ngOnInit() {
        this.storage.clearKey('friends_info');
        const friendsInfo = new src_app_models_friend_model__WEBPACK_IMPORTED_MODULE_3__.FriendsInfo();
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__.KeepFriendsInfo({ data: friendsInfo })).subscribe(success => { });
    }
};
SubmissionConfirmatoinComponent.ctorParameters = () => [
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__.StorageService },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__.SocialSharing }
];
SubmissionConfirmatoinComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-submission-confirmatoin',
        template: _raw_loader_submission_confirmatoin_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_submission_confirmatoin_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SubmissionConfirmatoinComponent);



/***/ }),

/***/ 6830:
/*!************************************************!*\
  !*** ./src/app/pages/terms/terms.component.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TermsComponent": () => (/* binding */ TermsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_terms_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./terms.component.html */ 5282);
/* harmony import */ var _terms_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./terms.component.scss */ 8167);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let TermsComponent = class TermsComponent {
    constructor() { }
    ngOnInit() { }
};
TermsComponent.ctorParameters = () => [];
TermsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-terms',
        template: _raw_loader_terms_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_terms_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TermsComponent);



/***/ }),

/***/ 6599:
/*!********************************************************************!*\
  !*** ./src/app/pages/update-password/update-password.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdatePasswordComponent": () => (/* binding */ UpdatePasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_update_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./update-password.component.html */ 8697);
/* harmony import */ var _update_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-password.component.scss */ 1100);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_services_school_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/school-service.service */ 3273);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);











let UpdatePasswordComponent = class UpdatePasswordComponent {
    constructor(router, formBuilder, loadingController, authService) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.authService = authService;
        this.tempUserData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
        this.userData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
        this.ChangeForm = formBuilder.group({
            mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]],
            new_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
            new_confirm_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]
        });
    }
    ngOnInit() {
        this.profile$.subscribe(data => {
            this.userData = data;
            console.log("profileData", this.userData);
        });
        this.tempprofile$.subscribe(data => {
            this.tempUserData = data;
            console.log("Temp User Data", this.tempUserData);
        });
    }
    goToLogin() {
        this.router.navigate(['/login']);
    }
    change() {
        console.log(this.ChangeForm.value);
        this.ChangeForm.get('mobile').setValue(this.tempUserData.mobile);
        this.authService.updatePassword(this.ChangeForm.value).subscribe(success => {
            console.log(success);
            this.ChangeForm.reset();
            this.router.navigate(['/login']);
        }, err => {
        });
    }
};
UpdatePasswordComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.LoadingController },
    { type: src_app_services_school_service_service__WEBPACK_IMPORTED_MODULE_4__.SchoolServiceService }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__.ProfileState.tempUserKey)
], UpdatePasswordComponent.prototype, "tempprofile$", void 0);
(0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_5__.ProfileState.profileKey)
], UpdatePasswordComponent.prototype, "profile$", void 0);
UpdatePasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-update-password',
        template: _raw_loader_update_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_update_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], UpdatePasswordComponent);



/***/ }),

/***/ 3834:
/*!**********************************************************!*\
  !*** ./src/app/pages/verify-otp/verify-otp.component.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VerifyOtpComponent": () => (/* binding */ VerifyOtpComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_verify_otp_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./verify-otp.component.html */ 492);
/* harmony import */ var _verify_otp_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./verify-otp.component.scss */ 8361);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);









let VerifyOtpComponent = class VerifyOtpComponent {
    constructor(router, alertController, store) {
        this.router = router;
        this.alertController = alertController;
        this.store = store;
        this.one = '';
        this.two = '';
        this.three = '';
        this.four = '';
        this.tempUserData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
        this.userData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
    }
    ngOnInit() {
        this.profile$.subscribe(data => {
            this.userData = data;
            console.log("User Data", this.userData);
        });
        this.tempprofile$.subscribe(data => {
            this.tempUserData = data;
            console.log("Temp User Data", this.tempUserData);
        });
    }
    verify() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log("verify");
            // this.store.dispatch(new TempProfile({data: profileData})).subscribe(success => {});
            let otp = this.one.concat(this.two, this.three, this.four);
            if (this.userData.otp == otp) {
                if (this.tempUserData.action == 'forget') {
                    this.router.navigate(['/update-password']);
                }
                else {
                    this.router.navigate(['/create-password']);
                }
            }
            else {
                const alert = yield this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: 'Error',
                    subHeader: '',
                    message: "OTP is wrong.",
                    buttons: ['OK']
                });
                yield alert.present();
                this.text1.setFocus();
                this.one = '';
                this.two = '';
                this.three = '';
                this.four = '';
            }
        });
    }
    clickEvent(fromtext, element) {
        var length = element.value.length;
        console.log('length', length);
        if (fromtext == 'text1' && length == 1) {
            this.text2.setFocus();
        }
        else if (fromtext == 'text2' && length == 1) {
            this.text3.setFocus();
        }
        else if (fromtext == 'text3' && length == 1) {
            this.text4.setFocus();
        }
    }
    sendOtp() {
        this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.SendOTP({ data: this.tempUserData })).subscribe(success => { });
    }
};
VerifyOtpComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.AlertController },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
VerifyOtpComponent.propDecorators = {
    text1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['text1',] }],
    text2: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['text2',] }],
    text3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['text3',] }],
    text4: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['text4',] }]
};
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.tempUserKey)
], VerifyOtpComponent.prototype, "tempprofile$", void 0);
(0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Select)(src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.ProfileState.profileKey)
], VerifyOtpComponent.prototype, "profile$", void 0);
VerifyOtpComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-verify-otp',
        template: _raw_loader_verify_otp_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_verify_otp_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], VerifyOtpComponent);



/***/ }),

/***/ 1808:
/*!*********************************************************!*\
  !*** ./src/app/pages/welcome/landing-page.component.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LandingPageComponent": () => (/* binding */ LandingPageComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_landing_page_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./landing-page.component.html */ 6226);
/* harmony import */ var _landing_page_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./landing-page.component.scss */ 8141);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/school-model */ 5039);
/* harmony import */ var src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/store/profile.actions */ 3069);








let LandingPageComponent = class LandingPageComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.name = '';
        this.mobile = '';
        this.email = '';
        this.type = '';
    }
    ngOnInit() { }
    submit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            let profileData = new src_app_models_school_model__WEBPACK_IMPORTED_MODULE_3__.Profile();
            console.log(profileData);
            profileData.type = this.type;
            profileData.mobile = this.mobile;
            profileData.email = this.email;
            profileData.name = this.name;
            profileData.action = 'register';
            this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.SendOTP({ data: profileData })).subscribe(success => {
                console.log("profileData", profileData);
                this.store.dispatch(new src_app_store_profile_actions__WEBPACK_IMPORTED_MODULE_4__.TempProfile({ data: profileData })).subscribe(success => {
                    this.router.navigate(['/verification']);
                });
            });
        });
    }
    login() {
        this.router.navigate(['/login']);
    }
};
LandingPageComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__.Store }
];
LandingPageComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-landing-page',
        template: _raw_loader_landing_page_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_landing_page_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LandingPageComponent);



/***/ }),

/***/ 3273:
/*!****************************************************!*\
  !*** ./src/app/services/school-service.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchoolServiceService": () => (/* binding */ SchoolServiceService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var _constant__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constant */ 2831);
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./storage.service */ 1188);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ 2340);








let SchoolServiceService = class SchoolServiceService {
    constructor(storageService, http, storage, store, injector) {
        this.storageService = storageService;
        this.http = http;
        this.storage = storage;
        this.store = store;
        this.injector = injector;
    }
    getFriendsInfo(searchTxt) {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'friendsinfo', { params: { txt: searchTxt } });
    }
    addFriendsInfo(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'friendsinfo', obj);
    }
    login(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'login', obj);
    }
    registration(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'register', obj);
    }
    sendOTP(profileObj) {
        console.log('project object = ', profileObj);
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'send_otp', profileObj);
    }
    verifyOTP(profileObj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'verify_otp', profileObj);
    }
    updateProfile(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'update_profile', obj);
    }
    changePassword(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'change_password', obj);
    }
    updatePassword(obj) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.baseUrl + 'update_password', obj);
    }
    logOut() {
        this.storage.clearKey(_constant__WEBPACK_IMPORTED_MODULE_1__.Constant.myLoginData);
        const store = this.injector.get(_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Store);
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.of)([]);
    }
};
SchoolServiceService.ctorParameters = () => [
    { type: _storage_service__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient },
    { type: _storage_service__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Store },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Injector }
];
SchoolServiceService = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], SchoolServiceService);



/***/ }),

/***/ 1188:
/*!*********************************************!*\
  !*** ./src/app/services/storage.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 2208);
/**
* Storage Services
* @author    ThemesBuckets <themebucketbd@gmail.com>
* @copyright Copyright (c) 2020
* @license   ThemesBuckets
*/



const { Storage } = _capacitor_core__WEBPACK_IMPORTED_MODULE_0__.Plugins;
let StorageService = class StorageService {
    constructor() { }
    getObject(ITEMS_KEY) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const ret = yield Storage.get({ key: ITEMS_KEY });
            const data = JSON.parse(ret.value);
            return data;
        });
    }
    setObject(profile, ITEMS_KEY) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            yield Storage.set({
                key: ITEMS_KEY,
                value: JSON.stringify(profile)
            });
        });
    }
    setProfile(profile, ITEMS_KEY) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            yield Storage.set({
                key: ITEMS_KEY,
                value: JSON.stringify(profile)
            });
        });
    }
    clearKey(ITEMS_KEY) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            yield Storage.remove({
                key: ITEMS_KEY
            });
        });
    }
    clear() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            yield Storage.clear();
        });
    }
};
StorageService.ctorParameters = () => [];
StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], StorageService);



/***/ }),

/***/ 3069:
/*!******************************************!*\
  !*** ./src/app/store/profile.actions.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SendOTP": () => (/* binding */ SendOTP),
/* harmony export */   "VerifyOTP": () => (/* binding */ VerifyOTP),
/* harmony export */   "AddProfile": () => (/* binding */ AddProfile),
/* harmony export */   "UpdateProfile": () => (/* binding */ UpdateProfile),
/* harmony export */   "SetProfile": () => (/* binding */ SetProfile),
/* harmony export */   "ViewProfile": () => (/* binding */ ViewProfile),
/* harmony export */   "SelectedModel": () => (/* binding */ SelectedModel),
/* harmony export */   "GetProfile": () => (/* binding */ GetProfile),
/* harmony export */   "ClearProfile": () => (/* binding */ ClearProfile),
/* harmony export */   "GetDashboard": () => (/* binding */ GetDashboard),
/* harmony export */   "Redirect": () => (/* binding */ Redirect),
/* harmony export */   "SendMessage": () => (/* binding */ SendMessage),
/* harmony export */   "TempProfile": () => (/* binding */ TempProfile),
/* harmony export */   "GetFriendsInfo": () => (/* binding */ GetFriendsInfo),
/* harmony export */   "AddFriendsInfo": () => (/* binding */ AddFriendsInfo),
/* harmony export */   "KeepFriendsInfo": () => (/* binding */ KeepFriendsInfo),
/* harmony export */   "ProfileState": () => (/* binding */ ProfileState)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngxs/store */ 3394);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 8307);
/* harmony import */ var _models_school_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/school-model */ 5039);
/* harmony import */ var _services_school_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/school-service.service */ 3273);
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/storage.service */ 1188);
/* harmony import */ var _constant__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constant */ 2831);
/* harmony import */ var _models_friend_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/friend-model */ 2692);
var ProfileState_1;










class SendOTP {
    constructor(payload) {
        this.payload = payload;
    }
}
SendOTP.type = 'Send OTP';
class VerifyOTP {
    constructor(payload) {
        this.payload = payload;
    }
}
VerifyOTP.type = 'Verify OTP';
class AddProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
AddProfile.type = 'New Registration';
class UpdateProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
UpdateProfile.type = 'Update User Data On Server';
class SetProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
SetProfile.type = 'Set Profile';
class ViewProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
ViewProfile.type = 'View Profile';
class SelectedModel {
    constructor(payload) {
        this.payload = payload;
    }
}
SelectedModel.type = 'Selected Model';
class GetProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
GetProfile.type = 'Login User';
class ClearProfile {
}
ClearProfile.type = 'Logout User';
class GetDashboard {
}
GetDashboard.type = 'Get Dashboard';
class Redirect {
}
Redirect.type = 'Redirect';
class SendMessage {
    constructor(payload) {
        this.payload = payload;
    }
}
SendMessage.type = 'Send Message';
class TempProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
TempProfile.type = 'Temp Profile';
class GetFriendsInfo {
    constructor(payload) {
        this.payload = payload;
    }
}
GetFriendsInfo.type = 'Get Friends Info';
class AddFriendsInfo {
    constructor(payload) {
        this.payload = payload;
    }
}
AddFriendsInfo.type = 'Add Friends Info';
class KeepFriendsInfo {
    constructor(payload) {
        this.payload = payload;
    }
}
KeepFriendsInfo.type = 'Keep Friends Info';
let ProfileState = ProfileState_1 = class ProfileState {
    constructor(store, router, storageService, authService) {
        this.store = store;
        this.router = router;
        this.storageService = storageService;
        this.authService = authService;
        console.log('Profile state constructor..');
    }
    static tempFriendInfoKey(state) {
        return state.temp_friend_info;
    }
    static friendsInfoKey(state) {
        return state.friends_info;
    }
    static tempUserKey(state) {
        return state.tempUser;
    }
    static profileKey(state) {
        return state.user;
    }
    static accesstokenKey(state) {
        return state.access_token;
    }
    static selectedProfileKey(state) {
        return state.selectedProfile;
    }
    static isAuthenticated(state) {
        return !!state.access_token;
    }
    keepFriendInfo(ctx, action) {
        const state = ctx.getState();
        ctx.patchState(Object.assign(Object.assign({}, state), { temp_friend_info: action.payload.data }));
    }
    getFriendInfo(ctx, action) {
        const state = ctx.getState();
        return this.authService.getFriendsInfo(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            ctx.patchState(Object.assign(Object.assign({}, state), { friends_info: res.data }));
        }));
    }
    addFriendInfo(ctx, action) {
        const state = ctx.getState();
        return this.authService.addFriendsInfo(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            ctx.patchState(Object.assign(Object.assign({}, state), { friends_info: res.data }));
        }));
    }
    tempProfile(ctx, action) {
        const state = ctx.getState();
        ctx.patchState(Object.assign(Object.assign({}, state), { tempUser: action.payload.data }));
    }
    sendOTP(ctx, action) {
        const state = ctx.getState();
        return this.authService.sendOTP(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            console.log("response otp : ", res);
            let userData = new _models_school_model__WEBPACK_IMPORTED_MODULE_1__.Profile();
            userData.otp = res.data;
            userData.email = action.payload.data.email;
            ctx.patchState(Object.assign(Object.assign({}, state), { user: userData }));
        }));
    }
    verifyOTP(ctx, action) {
        const state = ctx.getState();
        return this.authService.verifyOTP(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            console.log('after verify ', res);
            ctx.patchState(Object.assign(Object.assign({}, state), { user: res.data }));
        }));
    }
    addProfile(ctx, action) {
        const state = ctx.getState();
        return this.authService.registration(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            ctx.patchState(Object.assign(Object.assign({}, state), { user: res.data }));
        }));
    }
    setProfile(ctx, action) {
        const state = ctx.getState();
        console.log('data before store in local storage', action.payload.data);
        this.storageService.setObject(action.payload.data, _constant__WEBPACK_IMPORTED_MODULE_4__.Constant.myLoginData);
        ctx.patchState(Object.assign(Object.assign({}, state), { user: action.payload.data }));
    }
    updateProfile(ctx, action) {
        const state = ctx.getState();
        return this.authService.updateProfile(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            this.storageService.getObject(_constant__WEBPACK_IMPORTED_MODULE_4__.Constant.myLoginData).then(localLoginData => {
                localLoginData.user = res.data;
                this.storageService.setObject(localLoginData, _constant__WEBPACK_IMPORTED_MODULE_4__.Constant.myLoginData);
                ctx.patchState(Object.assign(Object.assign({}, state), { user: localLoginData.user, access_token: localLoginData.access_token }));
                this.ngxsAfterBootstrap(ctx);
            });
        }));
    }
    getProfile(ctx, action) {
        const state = ctx.getState();
        return this.authService.login(action.payload.data).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(res => {
            console.log('after login', res);
            this.storageService.setObject(res.data, _constant__WEBPACK_IMPORTED_MODULE_4__.Constant.myLoginData).then(suc => {
                ctx.patchState(Object.assign(Object.assign({}, state), { user: res.data.user, access_token: res.data.access_token }));
                this.ngxsAfterBootstrap(ctx);
            });
        }));
    }
    clearProfile(ctx, action) {
        const state = ctx.getState();
        return this.authService.logOut().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.tap)(data => {
            ctx.setState(Object.assign(Object.assign({}, state), { user: new _models_school_model__WEBPACK_IMPORTED_MODULE_1__.Profile(), access_token: '' }));
        }));
    }
    // LifeCycles
    ngxsOnInit(ctx) {
        this.user$.subscribe(data => {
            console.log("init data", data);
        });
        const state = ctx.getState();
        this.storageService.getObject(_constant__WEBPACK_IMPORTED_MODULE_4__.Constant.myLoginData).then((data) => {
            if (data !== null) {
                return ctx.setState(Object.assign(Object.assign({}, state), { user: data.user, access_token: data.access_token }));
            }
            this.store.dispatch(new Redirect());
        });
    }
    ngxsOnChanges(change) {
        // console.log('prev state', change.previousValue);
        // console.log('next state', change.currentValue);
    }
    ngxsAfterBootstrap(ctx) {
        const state = ctx.getState();
        console.log('state', state);
        if (!!state.user.id) {
            if (state.user.type == 'parent') {
                this.router.navigate(['/search-children']);
            }
            else {
                // this.router.navigate(['/review-info']);
                this.router.navigate(['/campus-aonoymous']);
            }
        }
        else {
            this.router.navigate(['/landing']);
        }
    }
};
ProfileState.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Store },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _services_school_service_service__WEBPACK_IMPORTED_MODULE_2__.SchoolServiceService }
];
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Select)(ProfileState_1)
], ProfileState.prototype, "user$", void 0);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(KeepFriendsInfo)
], ProfileState.prototype, "keepFriendInfo", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(GetFriendsInfo)
], ProfileState.prototype, "getFriendInfo", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(AddFriendsInfo)
], ProfileState.prototype, "addFriendInfo", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(TempProfile)
], ProfileState.prototype, "tempProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(SendOTP)
], ProfileState.prototype, "sendOTP", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(VerifyOTP)
], ProfileState.prototype, "verifyOTP", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(AddProfile)
], ProfileState.prototype, "addProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(SetProfile)
], ProfileState.prototype, "setProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(UpdateProfile)
], ProfileState.prototype, "updateProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(GetProfile)
], ProfileState.prototype, "getProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Action)(ClearProfile)
], ProfileState.prototype, "clearProfile", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "tempFriendInfoKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "friendsInfoKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "tempUserKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "profileKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "accesstokenKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "selectedProfileKey", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.Selector)()
], ProfileState, "isAuthenticated", null);
ProfileState = ProfileState_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_ngxs_store__WEBPACK_IMPORTED_MODULE_0__.State)({
        name: 'profile',
        defaults: {
            tempUser: new _models_school_model__WEBPACK_IMPORTED_MODULE_1__.Profile(),
            selectedProfile: new _models_school_model__WEBPACK_IMPORTED_MODULE_1__.Profile(),
            user: new _models_school_model__WEBPACK_IMPORTED_MODULE_1__.Profile(),
            access_token: '',
            temp_friend_info: new _models_friend_model__WEBPACK_IMPORTED_MODULE_5__.FriendsInfo(),
            friends_info: []
        }
    }),
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)()
], ProfileState);



/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    // baseUrl: 'http://127.0.0.1:8000/api/v1/'
    baseUrl: 'http://schoolapi.clean-bowled.in/public/api/v1/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 4608);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 6747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 2340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-action-sheet.entry.js": [
		7321,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		6108,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		1489,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		305,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		5830,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		7757,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		392,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		6911,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		937,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		8695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		6034,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		8837,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		4195,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		1709,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		5931,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		4513,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		8056,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		862,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		7509,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		6272,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		1855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		8708,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		3527,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		4694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		9222,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		5277,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		9921,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		3122,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		1602,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		5174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7895,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		6164,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		7162,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		1374,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		7896,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		5043,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		7802,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		9072,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		2191,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		801,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		7110,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		431,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 19:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-menu ion-content {\n  --background: var(--ion-item-background, var(--ion-background-color, #fff));\n}\n\nion-menu.md ion-content {\n  --padding-start: 8px;\n  --padding-end: 8px;\n  --padding-top: 20px;\n  --padding-bottom: 20px;\n}\n\nion-menu.md ion-list {\n  padding: 20px 0;\n}\n\nion-menu.md ion-note {\n  margin-bottom: 30px;\n}\n\nion-menu.md ion-list-header,\nion-menu.md ion-note {\n  padding-left: 10px;\n}\n\nion-menu.md ion-list#inbox-list {\n  border-bottom: 1px solid var(--ion-color-step-150, #d7d8da);\n}\n\nion-menu.md ion-list#inbox-list ion-list-header {\n  font-size: 22px;\n  font-weight: 600;\n  min-height: 20px;\n}\n\nion-menu.md ion-list#labels-list ion-list-header {\n  font-size: 16px;\n  margin-bottom: 18px;\n  color: #757575;\n  min-height: 26px;\n}\n\nion-menu.md ion-item {\n  --padding-start: 10px;\n  --padding-end: 10px;\n  border-radius: 4px;\n}\n\nion-menu.md ion-item.selected {\n  --background: rgba(var(--ion-color-primary-rgb), 0.14);\n}\n\nion-menu.md ion-item.selected ion-icon {\n  color: var(--ion-color-primary);\n}\n\nion-menu.md ion-item ion-icon {\n  color: var(--ion-color-primary);\n}\n\nion-menu.md ion-item ion-label {\n  font-weight: 500;\n}\n\nion-menu.ios ion-content {\n  --padding-bottom: 20px;\n}\n\nion-menu.ios ion-list {\n  padding: 20px 0 0 0;\n}\n\nion-menu.ios ion-note {\n  line-height: 24px;\n  margin-bottom: 20px;\n}\n\nion-menu.ios ion-item {\n  --padding-start: 16px;\n  --padding-end: 16px;\n  --min-height: 50px;\n}\n\nion-menu.ios ion-item.selected ion-icon {\n  color: var(--ion-color-primary);\n}\n\nion-menu.ios ion-item ion-icon {\n  font-size: 24px;\n  color: #73849a;\n}\n\nion-menu.ios ion-list#labels-list ion-list-header {\n  margin-bottom: 8px;\n}\n\nion-menu.ios ion-list-header,\nion-menu.ios ion-note {\n  padding-left: 16px;\n  padding-right: 16px;\n}\n\nion-menu.ios ion-note {\n  margin-bottom: 8px;\n}\n\nion-note {\n  display: inline-block;\n  font-size: 16px;\n  color: var(--ion-color-medium-shade);\n}\n\nion-item.selected {\n  --color: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJFQUFBO0FBQ0Y7O0FBRUE7RUFDRSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UsZUFBQTtBQUNGOztBQUVBO0VBQ0UsbUJBQUE7QUFDRjs7QUFFQTs7RUFFRSxrQkFBQTtBQUNGOztBQUVBO0VBQ0UsMkRBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUVBLGdCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBRUEsbUJBQUE7RUFFQSxjQUFBO0VBRUEsZ0JBQUE7QUFIRjs7QUFNQTtFQUNFLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQUhGOztBQU1BO0VBQ0Usc0RBQUE7QUFIRjs7QUFNQTtFQUNFLCtCQUFBO0FBSEY7O0FBTUE7RUFDRSwrQkFBQTtBQUhGOztBQU1BO0VBQ0UsZ0JBQUE7QUFIRjs7QUFNQTtFQUNFLHNCQUFBO0FBSEY7O0FBTUE7RUFDRSxtQkFBQTtBQUhGOztBQU1BO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBQUhGOztBQU1BO0VBQ0UscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBSEY7O0FBTUE7RUFDRSwrQkFBQTtBQUhGOztBQU1BO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUFIRjs7QUFNQTtFQUNFLGtCQUFBO0FBSEY7O0FBTUE7O0VBRUUsa0JBQUE7RUFDQSxtQkFBQTtBQUhGOztBQU1BO0VBQ0Usa0JBQUE7QUFIRjs7QUFNQTtFQUNFLHFCQUFBO0VBQ0EsZUFBQTtFQUVBLG9DQUFBO0FBSkY7O0FBT0E7RUFDRSxpQ0FBQTtBQUpGIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1tZW51IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLCB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvciwgI2ZmZikpO1xufVxuXG5pb24tbWVudS5tZCBpb24tY29udGVudCB7XG4gIC0tcGFkZGluZy1zdGFydDogOHB4O1xuICAtLXBhZGRpbmctZW5kOiA4cHg7XG4gIC0tcGFkZGluZy10b3A6IDIwcHg7XG4gIC0tcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1saXN0IHtcbiAgcGFkZGluZzogMjBweCAwO1xufVxuXG5pb24tbWVudS5tZCBpb24tbm90ZSB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1saXN0LWhlYWRlcixcbmlvbi1tZW51Lm1kIGlvbi1ub3RlIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuXG5pb24tbWVudS5tZCBpb24tbGlzdCNpbmJveC1saXN0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1zdGVwLTE1MCwgI2Q3ZDhkYSk7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1saXN0I2luYm94LWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xuXG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1saXN0I2xhYmVscy1saXN0IGlvbi1saXN0LWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMTZweDtcblxuICBtYXJnaW4tYm90dG9tOiAxOHB4O1xuXG4gIGNvbG9yOiAjNzU3NTc1O1xuXG4gIG1pbi1oZWlnaHQ6IDI2cHg7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAtLXBhZGRpbmctZW5kOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1pdGVtLnNlbGVjdGVkIHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXJnYiksIDAuMTQpO1xufVxuXG5pb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCBpb24taWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbmlvbi1tZW51Lm1kIGlvbi1pdGVtIGlvbi1pY29uIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuaW9uLW1lbnUubWQgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuaW9uLW1lbnUuaW9zIGlvbi1jb250ZW50IHtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cblxuaW9uLW1lbnUuaW9zIGlvbi1saXN0IHtcbiAgcGFkZGluZzogMjBweCAwIDAgMDtcbn1cblxuaW9uLW1lbnUuaW9zIGlvbi1ub3RlIHtcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbmlvbi1tZW51LmlvcyBpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMTZweDtcbiAgLS1wYWRkaW5nLWVuZDogMTZweDtcbiAgLS1taW4taGVpZ2h0OiA1MHB4O1xufVxuXG5pb24tbWVudS5pb3MgaW9uLWl0ZW0uc2VsZWN0ZWQgaW9uLWljb24ge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG5pb24tbWVudS5pb3MgaW9uLWl0ZW0gaW9uLWljb24ge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGNvbG9yOiAjNzM4NDlhO1xufVxuXG5pb24tbWVudS5pb3MgaW9uLWxpc3QjbGFiZWxzLWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xufVxuXG5pb24tbWVudS5pb3MgaW9uLWxpc3QtaGVhZGVyLFxuaW9uLW1lbnUuaW9zIGlvbi1ub3RlIHtcbiAgcGFkZGluZy1sZWZ0OiAxNnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xufVxuXG5pb24tbWVudS5pb3MgaW9uLW5vdGUge1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG59XG5cbmlvbi1ub3RlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE2cHg7XG5cbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xufVxuXG5pb24taXRlbS5zZWxlY3RlZCB7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn0iXX0= */");

/***/ }),

/***/ 7752:
/*!**********************************************************************!*\
  !*** ./src/app/pages/change-password/change-password.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".top-space {\n  margin-top: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0oiLCJmaWxlIjoiY2hhbmdlLXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvcC1zcGFjZSB7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbn0iXX0= */");

/***/ }),

/***/ 5157:
/*!**********************************************************************!*\
  !*** ./src/app/pages/children-detail/children-detail.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGlsZHJlbi1kZXRhaWwuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 3174:
/*!**************************************************************************!*\
  !*** ./src/app/pages/confirm-and-share/confirm-and-share.component.scss ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#notifications .unread {\n  --background: var(--ion-color-light);\n}\n\n.section_second {\n  height: 10%;\n  text-align: center;\n}\n\n.section_second p {\n  font-size: 20px;\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbmZpcm0tYW5kLXNoYXJlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksb0NBQUE7QUFBUjs7QUFJQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQURKOztBQUdJO0VBQ0ksZUFBQTtFQU9BLDhCQUFBO0VBQ0EsaUJBQUE7QUFQUiIsImZpbGUiOiJjb25maXJtLWFuZC1zaGFyZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNub3RpZmljYXRpb25zIHtcbiAgICAudW5yZWFkIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgIH1cbn1cblxuLnNlY3Rpb25fc2Vjb25kIHtcbiAgICBoZWlnaHQ6IDEwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAvLyBmb250LXdlaWdodDogOTAwO1xuXG4gICAgICAgIC8vIHBhZGRpbmctdG9wOiAyJTtcbiAgICBcblxuICAgIFxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWNvbG9yMSk7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ 4906:
/*!************************************************************************!*\
  !*** ./src/app/pages/confirm-to-share/confirm-to-share.component.scss ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#products .card-img {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center center;\n}\n#products #grid .card-img {\n  height: 150px;\n}\n#products #oneColumn .card-img {\n  height: 200px;\n}\n#products .product-name {\n  font-size: 1.2rem;\n  color: rgba(8, 8, 8, 0.58);\n}\n#products .previous-price {\n  text-decoration: line-through;\n  font-size: 0.8rem;\n}\n#products #list .item ion-thumbnail {\n  width: 140px;\n  height: 90px;\n}\n#products #list .item ion-thumbnail img {\n  width: 140px;\n  height: 90px;\n  border-radius: 5px;\n}\n#products #list .item ion-note {\n  font-size: 1.3rem;\n  font-weight: 600;\n}\n#products ion-fab-button {\n  --background: transparent;\n  --background-focused: transparent;\n  --background-activated: transparent;\n}\n.section_second {\n  height: 10%;\n  text-align: center;\n}\n.section_second p {\n  font-size: 20px;\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbmZpcm0tdG8tc2hhcmUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0NBQUE7QUFBUjtBQUdJO0VBQ0ksYUFBQTtBQURSO0FBSUk7RUFDSSxhQUFBO0FBRlI7QUFLSTtFQUNJLGlCQUFBO0VBQ0EsMEJBQUE7QUFIUjtBQU1JO0VBQ0ksNkJBQUE7RUFDQSxpQkFBQTtBQUpSO0FBU1k7RUFDSSxZQUFBO0VBQ0EsWUFBQTtBQVBoQjtBQVNnQjtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFQcEI7QUFXWTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7QUFUaEI7QUFjSTtFQUNJLHlCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQ0FBQTtBQVpSO0FBZ0JBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FBYko7QUFlSTtFQUNJLGVBQUE7RUFPQSw4QkFBQTtFQUNBLGlCQUFBO0FBbkJSIiwiZmlsZSI6ImNvbmZpcm0tdG8tc2hhcmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjcHJvZHVjdHMge1xuICAgIC5jYXJkLWltZyB7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gICAgfVxuXG4gICAgI2dyaWQgLmNhcmQtaW1nIHtcbiAgICAgICAgaGVpZ2h0OiAxNTBweDtcbiAgICB9XG5cbiAgICAjb25lQ29sdW1uIC5jYXJkLWltZyB7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgfVxuXG4gICAgLnByb2R1Y3QtbmFtZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgICAgICBjb2xvcjogcmdiYSg4LCA4LCA4LCAwLjU4KTtcbiAgICB9XG5cbiAgICAucHJldmlvdXMtcHJpY2Uge1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbiAgICAgICAgZm9udC1zaXplOiAwLjhyZW07XG4gICAgfVxuXG4gICAgI2xpc3Qge1xuICAgICAgICAuaXRlbSB7XG4gICAgICAgICAgICBpb24tdGh1bWJuYWlsIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTQwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA5MHB4O1xuXG4gICAgICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE0MHB4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDkwcHg7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1ub3RlIHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuM3JlbTtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgaW9uLWZhYi1idXR0b24ge1xuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHRyYW5zcGFyZW50O1xuICAgIH1cbn1cblxuLnNlY3Rpb25fc2Vjb25kIHtcbiAgICBoZWlnaHQ6IDEwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICBwIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAvLyBmb250LXdlaWdodDogOTAwO1xuXG4gICAgICAgIC8vIHBhZGRpbmctdG9wOiAyJTtcbiAgICBcblxuICAgIFxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWNvbG9yMSk7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ 385:
/*!************************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.component.scss ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb250YWN0LXVzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ 2830:
/*!**********************************************************************!*\
  !*** ./src/app/pages/create-password/create-password.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-button {\n  margin-top: 20px;\n}\n\n.center {\n  text-align: center;\n}\n\n.landing-page .section_one {\n  height: 15%;\n  margin-top: 80px;\n}\n\n.landing-page .section_one h2 {\n  font-weight: 900;\n  padding-top: 5%;\n}\n\n.landing-page .section_one p {\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n\n.landing-page .section_two {\n  height: 30%;\n}\n\n.landing-page .section_three {\n  height: 55%;\n  padding: 10px;\n}\n\n.landing-page .section_three p {\n  font-size: 1rem;\n  color: #6d6b6b;\n}\n\n.landing-page a.btn-social,\n.landing-page .btn-social {\n  border-radius: 50%;\n  color: #ffffff !important;\n  display: inline-block;\n  height: 54px;\n  line-height: 54px;\n  margin: 8px 4px;\n  text-align: center;\n  text-decoration: none;\n  transition: background-color 0.3s;\n  width: 54px;\n}\n\n.landing-page .btn-facebook {\n  background-color: #3b5998;\n}\n\n.landing-page .btn-google-plus {\n  background-color: #dd4b39;\n}\n\n.landing-page .btn-instagram {\n  background-color: #3f729b;\n}\n\n.landing-page .btn-twitter {\n  background-color: #55acee;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNyZWF0ZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtBQUVKOztBQUVJO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FBQ1I7O0FBQ1E7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFDWjs7QUFFUTtFQUNJLDhCQUFBO0VBQ0EsaUJBQUE7QUFBWjs7QUFJSTtFQUNJLFdBQUE7QUFGUjs7QUFLSTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FBSFI7O0FBS1E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQUhaOztBQU9JOztFQUVJLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUNBQUE7RUFDQSxXQUFBO0FBTFI7O0FBUUk7RUFDSSx5QkFBQTtBQU5SOztBQVNJO0VBQ0kseUJBQUE7QUFQUjs7QUFVSTtFQUNJLHlCQUFBO0FBUlI7O0FBV0k7RUFDSSx5QkFBQTtBQVRSIiwiZmlsZSI6ImNyZWF0ZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG4uY2VudGVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubGFuZGluZy1wYWdlIHtcblxuICAgIC5zZWN0aW9uX29uZSB7XG4gICAgICAgIGhlaWdodDogMTUlO1xuICAgICAgICBtYXJnaW4tdG9wOiA4MHB4O1xuXG4gICAgICAgIGgyIHtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgICAgIH1cblxuICAgICAgICBwIHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItY29sb3IxKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnNlY3Rpb25fdHdvIHtcbiAgICAgICAgaGVpZ2h0OiAzMCU7XG4gICAgfVxuXG4gICAgLnNlY3Rpb25fdGhyZWUge1xuICAgICAgICBoZWlnaHQ6IDU1JTtcbiAgICAgICAgcGFkZGluZzogMTBweDtcblxuICAgICAgICBwIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICAgIGNvbG9yOiAjNmQ2YjZiO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYS5idG4tc29jaWFsLFxuICAgIC5idG4tc29jaWFsIHtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIGhlaWdodDogNTRweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDU0cHg7XG4gICAgICAgIG1hcmdpbjogOHB4IDRweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgLjNzO1xuICAgICAgICB3aWR0aDogNTRweDtcbiAgICB9XG5cbiAgICAuYnRuLWZhY2Vib29rIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcbiAgICB9XG5cbiAgICAuYnRuLWdvb2dsZS1wbHVzIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RkNGIzOTtcbiAgICB9XG5cbiAgICAuYnRuLWluc3RhZ3JhbSB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzZjcyOWI7XG4gICAgfVxuXG4gICAgLmJ0bi10d2l0dGVyIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1YWNlZTtcbiAgICB9XG5cbn0iXX0= */");

/***/ }),

/***/ 5047:
/*!**********************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-button {\n  margin-top: 20px;\n}\n\nform {\n  margin-top: 120px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcmdldC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKIiwiZmlsZSI6ImZvcmdldC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbmZvcm0ge1xuICAgIG1hcmdpbi10b3A6IDEyMHB4O1xufSJdfQ== */");

/***/ }),

/***/ 6186:
/*!********************************************************************!*\
  !*** ./src/app/pages/friends-info/campus-anonymous.component.scss ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-item, ion-button {\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhbXB1cy1hbm9ueW1vdXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6ImNhbXB1cy1hbm9ueW1vdXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSwgaW9uLWJ1dHRvbiB7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ 9073:
/*!****************************************************************!*\
  !*** ./src/app/pages/how-it-works/how-it-works.component.scss ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#category ion-list {\n  margin-bottom: 0px !important;\n}\n#category .item ion-thumbnail {\n  width: 140px;\n  height: 90px;\n}\n#category .item ion-thumbnail img {\n  width: 140px;\n  height: 90px;\n  border-radius: 5px;\n}\n#category .item h2 {\n  font-size: 1.5rem;\n}\n.section_one {\n  text-align: center;\n  height: 10%;\n}\n.section_one h2 {\n  font-weight: 900;\n  margin-top: 0px;\n}\n.section_one h3 {\n  font-size: 20px;\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n.section_one p {\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvdy1pdC13b3Jrcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLDZCQUFBO0FBQVI7QUFJUTtFQUNJLFlBQUE7RUFDQSxZQUFBO0FBRlo7QUFJWTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFGaEI7QUFNUTtFQUNJLGlCQUFBO0FBSlo7QUFTQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQU5KO0FBUUk7RUFDSSxnQkFBQTtFQUVBLGVBQUE7QUFQUjtBQVdJO0VBR0ksZUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUFYUjtBQWVJO0VBRUksaUJBQUE7QUFkUiIsImZpbGUiOiJob3ctaXQtd29ya3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY2F0ZWdvcnkge1xuICAgIGlvbi1saXN0IHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgLml0ZW0ge1xuICAgICAgICBpb24tdGh1bWJuYWlsIHtcbiAgICAgICAgICAgIHdpZHRoOiAxNDBweDtcbiAgICAgICAgICAgIGhlaWdodDogOTBweDtcblxuICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTQwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA5MHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGgyIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uc2VjdGlvbl9vbmUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwJTtcblxuICAgIGgyIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogMiU7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcblxuICAgIH1cblxuICAgIGgzIHtcbiAgICBcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogMSU7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1jb2xvcjEpO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcblxuICAgIH1cblxuICAgIHAge1xuICAgICAgICAvL2NvbG9yOiB2YXIoLS1pb24tY29sb3ItY29sb3IxKTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxuICAgIFxufVxuIl19 */");

/***/ }),

/***/ 5100:
/*!**************************************************!*\
  !*** ./src/app/pages/login/login.component.scss ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 3871:
/*!****************************************************************************!*\
  !*** ./src/app/pages/not-ready-to-share/not-ready-to-share.component.scss ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub3QtcmVhZHktdG8tc2hhcmUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 7902:
/*!********************************************************************!*\
  !*** ./src/app/pages/privacy-policy/privacy-policy.component.scss ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcml2YWN5LXBvbGljeS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 8707:
/*!******************************************************!*\
  !*** ./src/app/pages/profile/profile.component.scss ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#account h2 {\n  font-size: 1.5rem;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2ZpbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0FBQVIiLCJmaWxlIjoicHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhY2NvdW50IHtcbiAgICBoMiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB9XG59Il19 */");

/***/ }),

/***/ 8214:
/*!********************************************************!*\
  !*** ./src/app/pages/register/register.component.scss ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#signup {\n  text-align: center;\n}\n#signup .section_one {\n  height: 10%;\n}\n#signup .section_one h2 {\n  font-weight: 900;\n  padding-top: 2%;\n}\n#signup .section_one p {\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n#signup .section_second {\n  height: 10%;\n}\n#signup .section_second h2 {\n  font-size: 20px;\n}\n#signup .section_second p {\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n#signup a.btn-social,\n#signup .btn-social {\n  border-radius: 50%;\n  color: #ffffff !important;\n  display: inline-block;\n  height: 35px;\n  line-height: 35px;\n  margin: 8px 4px;\n  text-align: center;\n  text-decoration: none;\n  transition: background-color 0.3s;\n  width: 35px;\n}\n#signup .btn-facebook {\n  background-color: #3b5998;\n}\n#signup .btn-google-plus {\n  background-color: #dd4b39;\n}\n#signup .btn-instagram {\n  background-color: #3f729b;\n}\n#signup .btn-twitter {\n  background-color: #55acee;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7QUFDSjtBQUNJO0VBQ0ksV0FBQTtBQUNSO0FBQ1E7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFDWjtBQUVRO0VBQ0ksOEJBQUE7RUFDQSxpQkFBQTtBQUFaO0FBSUk7RUFDSSxXQUFBO0FBRlI7QUFJUTtFQUNJLGVBQUE7QUFGWjtBQVFRO0VBQ0ksOEJBQUE7RUFDQSxpQkFBQTtBQU5aO0FBV0k7O0VBRUksa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxpQ0FBQTtFQUNBLFdBQUE7QUFUUjtBQVlJO0VBQ0kseUJBQUE7QUFWUjtBQWFJO0VBQ0kseUJBQUE7QUFYUjtBQWNJO0VBQ0kseUJBQUE7QUFaUjtBQWVJO0VBQ0kseUJBQUE7QUFiUiIsImZpbGUiOiJyZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNzaWdudXAge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgIC5zZWN0aW9uX29uZSB7XG4gICAgICAgIGhlaWdodDogMTAlO1xuXG4gICAgICAgIGgyIHtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMiU7XG4gICAgICAgIH1cblxuICAgICAgICBwIHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItY29sb3IxKTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnNlY3Rpb25fc2Vjb25kIHtcbiAgICAgICAgaGVpZ2h0OiAxMCU7XG5cbiAgICAgICAgaDIge1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgLy8gZm9udC13ZWlnaHQ6IDkwMDtcblxuICAgICAgICAgICAgLy8gcGFkZGluZy10b3A6IDIlO1xuICAgICAgICB9XG5cbiAgICAgICAgcCB7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWNvbG9yMSk7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcblxuICAgIGEuYnRuLXNvY2lhbCxcbiAgICAuYnRuLXNvY2lhbCB7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICAgICAgICBtYXJnaW46IDhweCA0cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kLWNvbG9yIC4zcztcbiAgICAgICAgd2lkdGg6IDM1cHg7XG4gICAgfVxuXG4gICAgLmJ0bi1mYWNlYm9vayB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzYjU5OTg7XG4gICAgfVxuXG4gICAgLmJ0bi1nb29nbGUtcGx1cyB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZDRiMzk7XG4gICAgfVxuXG4gICAgLmJ0bi1pbnN0YWdyYW0ge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y3MjliO1xuICAgIH1cblxuICAgIC5idG4tdHdpdHRlciB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NWFjZWU7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ 1331:
/*!**************************************************************!*\
  !*** ./src/app/pages/review-info/review-info.component.scss ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-button {\n  margin-top: 20px;\n}\n\nion-item {\n  height: 150px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJldmlldy1pbmZvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLHdCQUFBO0FBQ0oiLCJmaWxlIjoicmV2aWV3LWluZm8uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9uIHtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG5pb24taXRlbSB7XG4gICAgaGVpZ2h0OiAxNTBweCAhaW1wb3J0YW50O1xufVxuIl19 */");

/***/ }),

/***/ 869:
/*!******************************************************************!*\
  !*** ./src/app/pages/reward-option/reward-option.component.scss ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".box {\n  height: 80px;\n  background: #6b6a6a;\n  border-radius: 10px;\n  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n  text-align: center;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: white;\n  margin: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJld2FyZC1vcHRpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHdEQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQ0oiLCJmaWxlIjoicmV3YXJkLW9wdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib3gge1xuICAgIGhlaWdodDogODBweDtcbiAgICBiYWNrZ3JvdW5kOiAjNmI2YTZhO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwLjVyZW0gMXJlbSByZ2JhKDAsIDAsIDAsIDAuMTUpICFpbXBvcnRhbnQ7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luOiA1cHg7XG59XG5cbiJdfQ== */");

/***/ }),

/***/ 1311:
/*!**********************************************************************!*\
  !*** ./src/app/pages/search-children/search-children.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWFyY2gtY2hpbGRyZW4uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 6789:
/*!**************************************************************************************!*\
  !*** ./src/app/pages/submission-confirmatoin/submission-confirmatoin.component.scss ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#products .card-img {\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center center;\n}\n#products #grid .card-img {\n  height: 150px;\n}\n#products #oneColumn .card-img {\n  height: 200px;\n}\n#products .product-name {\n  font-size: 1.2rem;\n  color: rgba(8, 8, 8, 0.58);\n}\n#products .previous-price {\n  text-decoration: line-through;\n  font-size: 0.8rem;\n}\n#products #list .item ion-thumbnail {\n  width: 140px;\n  height: 90px;\n}\n#products #list .item ion-thumbnail img {\n  width: 140px;\n  height: 90px;\n  border-radius: 5px;\n}\n#products #list .item ion-note {\n  font-size: 1.3rem;\n  font-weight: 600;\n}\n#products ion-fab-button {\n  --background: transparent;\n  --background-focused: transparent;\n  --background-activated: transparent;\n}\n.section_second {\n  height: 10%;\n  text-align: center;\n}\n.section_second p {\n  font-size: 20px;\n  color: var(--ion-color-color1);\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1Ym1pc3Npb24tY29uZmlybWF0b2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGtDQUFBO0FBQVI7QUFHSTtFQUNJLGFBQUE7QUFEUjtBQUlJO0VBQ0ksYUFBQTtBQUZSO0FBS0k7RUFDSSxpQkFBQTtFQUNBLDBCQUFBO0FBSFI7QUFNSTtFQUNJLDZCQUFBO0VBQ0EsaUJBQUE7QUFKUjtBQVNZO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFQaEI7QUFTZ0I7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBUHBCO0FBV1k7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FBVGhCO0FBY0k7RUFDSSx5QkFBQTtFQUNBLGlDQUFBO0VBQ0EsbUNBQUE7QUFaUjtBQWdCQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQWJKO0FBZUk7RUFDSSxlQUFBO0VBT0EsOEJBQUE7RUFDQSxpQkFBQTtBQW5CUiIsImZpbGUiOiJzdWJtaXNzaW9uLWNvbmZpcm1hdG9pbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNwcm9kdWN0cyB7XG4gICAgLmNhcmQtaW1nIHtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICB9XG5cbiAgICAjZ3JpZCAuY2FyZC1pbWcge1xuICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIH1cblxuICAgICNvbmVDb2x1bW4gLmNhcmQtaW1nIHtcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICB9XG5cbiAgICAucHJvZHVjdC1uYW1lIHtcbiAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgICAgIGNvbG9yOiByZ2JhKDgsIDgsIDgsIDAuNTgpO1xuICAgIH1cblxuICAgIC5wcmV2aW91cy1wcmljZSB7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICAgICAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgICB9XG5cbiAgICAjbGlzdCB7XG4gICAgICAgIC5pdGVtIHtcbiAgICAgICAgICAgIGlvbi10aHVtYm5haWwge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxNDBweDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDkwcHg7XG5cbiAgICAgICAgICAgICAgICBpbWcge1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTQwcHg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogOTBweDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLW5vdGUge1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpb24tZmFiLWJ1dHRvbiB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiB0cmFuc3BhcmVudDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogdHJhbnNwYXJlbnQ7XG4gICAgfVxufVxuXG4uc2VjdGlvbl9zZWNvbmQge1xuICAgIGhlaWdodDogMTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIC8vIGZvbnQtd2VpZ2h0OiA5MDA7XG5cbiAgICAgICAgLy8gcGFkZGluZy10b3A6IDIlO1xuICAgIFxuXG4gICAgXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItY29sb3IxKTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ 8167:
/*!**************************************************!*\
  !*** ./src/app/pages/terms/terms.component.scss ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZXJtcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 1100:
/*!**********************************************************************!*\
  !*** ./src/app/pages/update-password/update-password.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGRhdGUtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 8361:
/*!************************************************************!*\
  !*** ./src/app/pages/verify-otp/verify-otp.component.scss ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-input {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlcmlmeS1vdHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQUNKIiwiZmlsZSI6InZlcmlmeS1vdHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taW5wdXQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLy8gI3ZlcmlmaWNhdGlvbiB7XG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gICAgIGhlaWdodDogMTAwJTtcblxuLy8gICAgIC5zZWN0aW9uX29uZSB7XG4vLyAgICAgICAgIGhlaWdodDogMTUlO1xuXG4vLyAgICAgICAgIGgyIHtcbi8vICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4vLyAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMiU7XG4vLyAgICAgICAgIH1cblxuLy8gICAgICAgICBwIHtcbi8vICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItY29sb3IxKTtcbi8vICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuXG4vLyAgICAgLnNlY3Rpb25fdHdvIHtcbi8vICAgICAgICAgaGVpZ2h0OiA3MCU7XG5cbi8vICAgICAgICAgaW1nIHtcbi8vICAgICAgICAgICAgIGhlaWdodDogNTAlO1xuLy8gICAgICAgICB9XG5cbi8vICAgICAgICAgaW9uLWl0ZW0ge1xuLy8gICAgICAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWNvbG9yMik7XG4vLyAgICAgICAgIH1cbi8vICAgICB9XG5cbi8vICAgICAuc2VjdGlvbl90aHJlZSB7XG4vLyAgICAgICAgIGhlaWdodDogMTUlO1xuLy8gICAgIH1cbi8vIH0iXX0= */");

/***/ }),

/***/ 8141:
/*!***********************************************************!*\
  !*** ./src/app/pages/welcome/landing-page.component.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-item, ion-button {\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxhbmRpbmctcGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0oiLCJmaWxlIjoibGFuZGluZy1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0sIGlvbi1idXR0b24ge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG4iXX0= */");

/***/ }),

/***/ 1106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app>\n  <ion-split-pane contentId=\"main-content\">\n    <ion-menu contentId=\"main-content\" type=\"overlay\">\n      <ion-content>\n        <ion-list class=\"side-menu\" lines=\"none\">\n          <ion-menu-toggle auto-hide=\"false\" lines=\"none\">\n\n            <!-- pro avatar -->\n            <ion-item class=\"header\" lines=\"none\">\n              <ion-label>\n                <span class=\"name\">{{ profileData.name }}</span>\n                <span class=\"email\">{{ profileData.email }}</span>\n              </ion-label>\n            </ion-item>\n            <!-- pro avatar -->\n\n            <ion-item *ngIf=\"profileData.type=='child'\" auto-hide=\"true\" routerDirection=\"root\" routerLink=\"/campus-aonoymous\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"person-outline\"></ion-icon>\n              <ion-label>Help Your Friend</ion-label>\n            </ion-item>\n            \n            <ion-item *ngIf=\"profileData.type=='parent'\" auto-hide=\"true\" routerDirection=\"root\" routerLink=\"/search-children\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"search-outline\"></ion-icon>\n              <ion-label>Search Child</ion-label>\n            </ion-item>\n\n            <ion-item routerDirection=\"root\" routerLink=\"/change-password\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"lock-closed-outline\"></ion-icon>\n              <ion-label>Change Password</ion-label>\n            </ion-item>\n  \n            <ion-item routerDirection=\"root\" routerLink=\"/terms\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"document-text-outline\"></ion-icon>\n              <ion-label>Terms of Use</ion-label>\n            </ion-item>\n\n            <ion-item routerDirection=\"root\" routerLink=\"/privacy-policy\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"shield-checkmark-outline\"></ion-icon>\n              <ion-label>Privacy Policy</ion-label>\n            </ion-item>\n\n            <ion-item routerDirection=\"root\" routerLink=\"/contact-us\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"chatbox-outline\"></ion-icon>\n              <ion-label>Contact Us</ion-label>\n            </ion-item>\n\n            <!-- Logout -->\n            <ion-item (click)=\"signOut()\" routerDirection=\"root\" lines=\"none\" detail=\"false\">\n              <ion-icon slot=\"start\" name=\"log-out-outline\"></ion-icon>\n              <ion-label>Logout</ion-label>\n            </ion-item>\n            <!-- ./ Logout /. -->\n\n          </ion-menu-toggle>\n\n        </ion-list>\n\n      </ion-content>\n      <ion-footer>\n        <ion-item routerDirection=\"root\" lines=\"none\" detail=\"false\">\n          <ion-icon slot=\"start\" name=\"phone-portrait-outline\"></ion-icon>\n          <ion-label>App Version 1.0</ion-label>\n        </ion-item>\n      </ion-footer>\n    </ion-menu>\n    <ion-router-outlet id=\"main-content\"></ion-router-outlet>\n  \n  </ion-split-pane>\n  \n</ion-app>\n");

/***/ }),

/***/ 2957:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/change-password/change-password.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header [translucent]=\"true\" class=\"ion-no-border\">\n  <ion-toolbar>\n    <!-- Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <!-- Title of the app -->\n    <ion-title>Change Password</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen=\"true\">\n\n  <div class=\"field-content top-space\">\n    <form class=\"ion-padding std-from\" [formGroup]=\"ChangeForm\">\n      <ion-list lines=\"none\">\n\n          <!-- Current Password -->\n          <ion-list-header>\n            <ion-label>Current Password<sup>*</sup></ion-label>\n          </ion-list-header>\n          <ion-item lines=\"none\">\n            <ion-input type=\"password\" placeholder=\"Current Password\" name=\"current_password\" formControlName=\"current_password\"></ion-input>\n          </ion-item>\n\n          <!-- New Password -->\n          <ion-list-header>\n            <ion-label>New Password<sup>*</sup></ion-label>\n          </ion-list-header>\n          <ion-item lines=\"none\">\n            <ion-input type=\"password\" placeholder=\"New Password\" name=\"new_password\" formControlName=\"new_password\"></ion-input>\n          </ion-item>\n\n          <!-- New Confirm Password -->\n          <ion-list-header>\n            <ion-label>Confirm Password<sup>*</sup></ion-label>\n          </ion-list-header>\n          <ion-item lines=\"none\">\n            <ion-input type=\"password\" placeholder=\"New Confirm Password\" name=\"new_confirm_password\" formControlName=\"new_confirm_password\"></ion-input>\n          </ion-item>\n\n      </ion-list>\n    </form>\n  </div>\n\n</ion-content>\n\n<ion-footer>\n  <div class=\"std-from\">\n    <ion-list lines=\"none\">\n      <ion-button expand=\"block\" (click)=\"change()\"> Update <i class=\"flaticon-right-arrow\"></i> </ion-button>\n    </ion-list>\n  </div>\n</ion-footer>");

/***/ }),

/***/ 7343:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/children-detail/children-detail.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"end\" (click)=\"dismiss()\">\n      <ion-icon class=\"flaticon-close flaticon-filter\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding std-from\">\n  <div class=\"\">\n    <br>\n    <ion-label><span class=\"link\">Sharing Information</span></ion-label>\n    <p *ngIf=\"friendInfoDetail.name\">Name : {{ friendInfoDetail.name }}</p>\n    <p *ngIf=\"friendInfoDetail.email\">Email : {{ friendInfoDetail.email }}</p>\n    <p *ngIf=\"friendInfoDetail.phone\">Phone : {{ friendInfoDetail.phone }}</p>\n    <p *ngIf=\"friendInfoDetail.country\">Country : {{ friendInfoDetail.country }}</p>\n    <p *ngIf=\"friendInfoDetail.state\">State : {{ friendInfoDetail.state }}</p>\n    <p *ngIf=\"friendInfoDetail.city\">City : {{ friendInfoDetail.city }}</p>\n    <p *ngIf=\"friendInfoDetail.message\">Message : {{ friendInfoDetail.message }}</p>\n    <br>\n    <ion-label><span class=\"link\">Reward Selected</span></ion-label>\n    <p>Amazon</p>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 5238:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/confirm-and-share/confirm-and-share.component.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\n  - Notification Screen\n  - @author    ThemesBuckets <themebucketbd@gmail.com>\n  - @copyright Copyright (c) 2020\n  - @license   ThemesBuckets\n-->\n\n<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Side Menu -->\n    \n    <ion-buttons slot=\"start\">\n      <ion-back-button routerLink=\"/ready-to-share\" default-href=\"\" text=\"\" icon=\"arrow-back\" routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n\n    <!-- Page Title -->\n    <ion-title>Confirm and Share</ion-title>\n\n  \n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content class=\"ion-padding\">\n\n  <div class=\"section_second\">\n    <p>Sharing Information</p>\n  </div>\n\n  <div>\n    <p>Reward Selected</p>\n  </div>\n\n  <div>\n    <p>A Thank You Card </p>\n  </div>\n  \n</ion-content>\n\n<ion-footer>\n  <ion-button expand=\"block\" routerLink=\"/\" routerDirection=\"root\">\n    <ion-text><strong>Submit</strong></ion-text>\n  </ion-button>\n</ion-footer>");

/***/ }),

/***/ 2887:
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/confirm-to-share/confirm-to-share.component.html ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n\n<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n      <!-- Side Menu Option -->\n      <ion-buttons slot=\"start\">\n          <ion-back-button></ion-back-button>\n      </ion-buttons>\n\n      <!-- Title of page -->\n      <ion-title>Confirm and Share</ion-title>\n\n  </ion-toolbar>\n</ion-header>\n\n\n<!-- Contents -->\n<ion-content class=\"ion-padding std-from\">\n  <div class=\"\">\n      <br>\n      <ion-label><span class=\"link\">Sharing Information</span></ion-label>\n      <p *ngIf=\"tempFriendInfo.name\">Name : {{ tempFriendInfo.name }}</p>\n      <p *ngIf=\"tempFriendInfo.email\">Email : {{ tempFriendInfo.email }}</p>\n      <p *ngIf=\"tempFriendInfo.phone\">Phone : {{ tempFriendInfo.phone }}</p>\n      <p *ngIf=\"tempFriendInfo.country\">Country : {{ tempFriendInfo.country }}</p>\n      <p *ngIf=\"tempFriendInfo.state\">State : {{ tempFriendInfo.state }}</p>\n      <p *ngIf=\"tempFriendInfo.city\">City : {{ tempFriendInfo.city }}</p>\n      <p *ngIf=\"tempFriendInfo.message\">Message : {{ tempFriendInfo.message }}</p>\n      <br>\n      <ion-label><span class=\"link\">Reward Selected</span></ion-label>\n      <p>Amazon</p>\n  </div>\n\n  <ion-list>\n    <br>\n    <ion-button expand=\"block\" (click)=\"submit()\">Submit</ion-button>\n    <br><br>\n  </ion-list>\n\n</ion-content>\n");

/***/ }),

/***/ 2470:
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-us/contact-us.component.html ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n\n\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\" routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n\n    <!-- Page tile -->\n    <ion-title>Contact Us</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Main Contents -->\n<ion-content id=\"category\" class=\"ion-padding\">\n\n</ion-content>\n");

/***/ }),

/***/ 4901:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/create-password/create-password.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Contents -->\n<ion-content class=\"landing-page\" fullscreen>\n  <div class=\"section_one center\">\n    <h2>Create Password</h2>\n    <p>Create your new password to login.</p>\n  </div>\n\n  <!-- SignIn Form -->\n  <form class=\"std-from\">\n    <ion-list lines=\"none\">\n      <!-- Password -->\n      <ion-list-header>\n        <ion-label>Password<sup>*</sup></ion-label>\n      </ion-list-header>\n      <ion-item>\n        <ion-input [(ngModel)]=\"password\" type=\"password\" placeholder=\"Password\" name=\"password\"></ion-input>\n      </ion-item>\n\n      <!-- Confirm Password -->\n      <ion-list-header>\n        <ion-label>Confirm Password<sup>*</sup></ion-label>\n      </ion-list-header>\n      <ion-item>\n        <ion-input [(ngModel)]=\"confirm_password\" type=\"password\" placeholder=\"Confirm Password\" name=\"confirm_password\"></ion-input>\n      </ion-item>\n\n      <ion-button expand=\"block\" (click)=\"submit()\">\n        <ion-text><strong>Submit</strong></ion-text>\n      </ion-button>\n\n    </ion-list>\n  </form>\n\n</ion-content>\n");

/***/ }),

/***/ 226:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forget-password/forget-password.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header fullscreen>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\" routerLink=\"/landing\"\n        routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content class=\"ion-no-padding auth-page\" fullscreen>\n\n  <div class=\"header\">\n    <h2>Campus Anonymous!</h2>\n    <h3>Forgot Password?</h3>\n  </div>\n  <!-- <div class=\"section_two ion-padding\">\n    <img src=\"assets/images/forgot-password.png\" width=\"70%\" />\n  </div> -->\n\n  <!-- Forget Password Form -->\n  <form class=\"std-from\">\n    <ion-list lines=\"none\">\n      <!-- Phone Number -->\n      <ion-list-header>\n        <ion-label>Phone Number<sup>*</sup></ion-label>\n      </ion-list-header>\n      <ion-item>\n        <ion-input type=\"number\" [(ngModel)]=\"phone\" placeholder=\"Phone Number\" name=\"phone\"></ion-input>\n      </ion-item>\n\n      <ion-button expand=\"block\" (click)=\"sendOtp()\"> Send OTP </ion-button>\n\n      <ion-list-header class=\"link ion-text-center\" routerLink=\"/landing\">\n        <ion-label>Don't have an account? <span>Register</span></ion-label>\n      </ion-list-header>\n    </ion-list>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ 1278:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/friends-info/campus-anonymous.component.html ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header [translucent]=\"true\" class=\"ion-no-border\">\n  <ion-toolbar>\n    <!-- Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <!-- Title of the app -->\n    <ion-title> Help your friend</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content  class=\"ion-padding std-from\" fullscreen>\n  <!-- getFromDraft() -->\n\n  <div class=\"section_one\">\n    <p >Who do you want to help today?  <br>\n           Provide your friend's email/phone number and their city/State/Country location where your friend is currently living.</p>\n    <ion-label *ngIf=\"tempFriendInfo.name != ''\">Having data in draft, <span class=\"link\" (click)=\"getFromDraft()\">get draft data</span>.</ion-label>\n  </div>\n\n\n  <ion-list lines=\"none\">\n    <!-- Friend's Name -->\n    <!-- <ion-list-header>\n      <ion-label>Name<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"Name\" name=\"name\" [(ngModel)]=\"name\"></ion-input>\n    </ion-item>\n\n\n    <!-- Friend's email address -->\n    <!-- <ion-list-header>\n      <ion-label>Email<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"email\" placeholder=\"Email\" name=\"email\" [(ngModel)]=\"email\"></ion-input>\n    </ion-item>\n\n\n    <!-- Friend's phone number with area code -->\n    <!-- <ion-list-header>\n      <ion-label>Phone Number<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"number\" placeholder=\"Phone\" name=\"phone\" [(ngModel)]=\"phone\"></ion-input>\n    </ion-item>\n\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"School\" name=\"school\" [(ngModel)]=\"school\"></ion-input>\n    </ion-item>\n\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"Grade\" name=\"grade\" [(ngModel)]=\"grade\"></ion-input>\n    </ion-item>\n\n    <!-- Country -->\n    <!-- <ion-list-header>\n      <ion-label>Country<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"Country\" name=\"country\" [(ngModel)]=\"country\"></ion-input>\n    </ion-item>\n\n    <!-- State -->\n    <!-- <ion-list-header>\n      <ion-label>State<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"State\" name=\"state\" [(ngModel)]=\"state\"></ion-input>\n    </ion-item>\n\n    <!-- city -->\n    <!-- <ion-list-header>\n      <ion-label>City<sup>*</sup></ion-label>\n    </ion-list-header> -->\n    <ion-item class=\"\">\n      <ion-input type=\"text\" placeholder=\"City\" name=\"city\" [(ngModel)]=\"city\"></ion-input>\n    </ion-item>\n\n    <ion-button class=\"btn-top\" expand=\"block\" (click)=\"share('notready')\">\n      <ion-text><strong>Not Ready to share</strong></ion-text>\n    </ion-button>\n\n    <ion-button expand=\"block\" (click)=\"share('ready')\">\n      <ion-text><strong>Ok, I am ready to share</strong></ion-text>\n    </ion-button>\n\n    <br><br>\n  </ion-list>\n\n</ion-content>");

/***/ }),

/***/ 6429:
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/how-it-works/how-it-works.component.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n\n\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\" routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n\n    <!-- Page tile -->\n    <ion-title>How it works?</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Main Contents -->\n<ion-content id=\"category\" class=\"ion-padding\">\n\n  <div class=\"section_one\">\n    <h2>Campus Anonymous</h2>\n    <h3>How it works <br>\n    </h3>\n    <p>Sign in to help a friend and collect your reward</p>\n  </div>\n\n  <p style=\"margin-top: 65px;\" >The App is for students who wish to help their friends by anonymously informing their friend's family about friend's troubling situation. <br> <br>\n    The App requires the family member to provide a reward and rating for the shared information.  Student chooses the reward option/amount.  <br> <br>\n    No identities are shared or revealed.  Family member receives only the shared text and nothing else. <br><br>\n    Information shared is encrypted and shared only with the family member <br><br>\n    Once information is accessed by the family member, the shared information is deleted from the system in 24 hours… poof! <br><br>\n\n  </p>\n\n  <p style=\"font-weight: bold;\" >How does Campus Anonymous afford to provide for this service?</p> \n  <p style=\"margin-top: 25px;\">We take a share of the reward.  The size of the share is based on the reporting student's feedback rating.  The higher the rating the less we take.  In other words, the more good you do, the more you get rewarded.  </p>\n  \n  <h3 style=\" text-align: center; font-size: 20px; color: var(--ion-color-color1); font-weight: bold;\" >Lets do good and prosper!</h3>\n\n</ion-content>\n");

/***/ }),

/***/ 911:
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.component.html ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Contents -->\n<ion-content class=\"ion-no-padding auth-page\" fullscreen>\n  <div class=\"header\">\n    <h2>\n      <span>Welcome to,</span>\n      Campus Anonymous!\n    </h2>\n    <!-- <p>For Students A Good Friend Indeed </p>\n    <h3>Sign in to help a friend and collect your reward</h3> -->\n    <h3>Login</h3>\n  </div>\n\n  <!-- SignIn Form -->\n  <form class=\"std-from\">\n    <ion-list lines=\"none\">\n      <!-- Email -->\n      <ion-list-header>\n        <ion-label>Username <sup>*</sup></ion-label>\n      </ion-list-header>\n      <ion-item>\n        <ion-input type=\"email\" [(ngModel)]=\"email\" placeholder=\"Email\" name=\"email\"></ion-input>\n      </ion-item>\n\n      <!-- Password -->\n      <ion-list-header>\n        <ion-label>Password <sup>*</sup></ion-label>\n      </ion-list-header>\n      <ion-item>\n        <ion-input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Password\" name=\"password\"></ion-input>\n      </ion-item>\n\n      <ion-list-header routerLink=\"/forget-password\" class=\"link ion-text-end\">\n        <ion-label><span>Forgot Password?</span></ion-label>\n      </ion-list-header>\n      <ion-button expand=\"block\" (click)=\"login()\"> Log in  </ion-button>\n      <ion-list-header class=\"link ion-text-center\" routerLink=\"/landing\">\n        <ion-label>Don't have an account? <span>Register</span></ion-label>\n      </ion-list-header>\n      <ion-list-header routerLink=\"/how-it-works\" class=\"link ion-text-center\">\n        <ion-label><span>How It Works?</span></ion-label>\n      </ion-list-header>\n\n    </ion-list>\n  </form>\n\n</ion-content>\n");

/***/ }),

/***/ 9814:
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/not-ready-to-share/not-ready-to-share.component.html ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n\n    <!-- Page title -->\n    <ion-title>Not Ready to share</ion-title>\n\n\n  </ion-toolbar>\n</ion-header>\n\n\n<!-- Contents -->\n<ion-content class=\"ion-padding std-from\" id=\"deal\">\n  <div class=\"section_one\">\n    <p>Here is your information. It is saved as a draft. You can continue from where you left off.</p>\n  </div>\n  <ion-label><span>Review Information</span></ion-label>\n  <p *ngIf=\"tempFriendInfo.name\">Name : {{ tempFriendInfo.name }}</p>\n  <p *ngIf=\"tempFriendInfo.email\">Email : {{ tempFriendInfo.email }}</p>\n  <p *ngIf=\"tempFriendInfo.phone\">Phone : {{ tempFriendInfo.phone }}</p>\n  <p *ngIf=\"tempFriendInfo.country\">Country : {{ tempFriendInfo.country }}</p>\n  <p *ngIf=\"tempFriendInfo.state\">State : {{ tempFriendInfo.state }}</p>\n  <p *ngIf=\"tempFriendInfo.city\">City : {{ tempFriendInfo.city }}</p>\n\n  <ion-list lines=\"none\">\n    <ion-button expand=\"block\" routerLink=\"/review-info\">\n      <ion-text><strong>Ok, I am ready </strong></ion-text>\n    </ion-button>\n    <br><br>\n  </ion-list>\n  <!-- <p style=\"font-weight: bold;\">to select my reward options and share the above information.</p> -->\n\n</ion-content>\n\n\n");

/***/ }),

/***/ 2209:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/privacy-policy/privacy-policy.component.html ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n\n\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\" routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n\n    <!-- Page tile -->\n    <ion-title>Privacy Policy</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Main Contents -->\n<ion-content id=\"category\" class=\"ion-padding\">\n\n</ion-content>\n");

/***/ }),

/***/ 9524:
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.component.html ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Side Menu -->\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n    <!-- Page title -->\n    <ion-title>Account</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content id=\"account\" class=\"ion-padding\">\n\n  <!-- User/Profile Infos -->\n  <ion-item lines=\"none\">\n    <!-- Avatar/Image of User -->\n    <ion-avatar slot=\"end\">\n      <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\n    </ion-avatar>\n\n    <!-- Name of User -->\n    <ion-label>\n      <h2>Alessia McKenzie</h2>\n      <p>Silver Member</p>\n    </ion-label>\n  </ion-item>\n\n  <!-- Some Menu Options -->\n  <ion-card>\n    <ion-grid>\n      <ion-row class=\"ion-text-center\">\n        <ion-col>\n          <ion-icon name=\"heart\" style=\"zoom:1.5;\"></ion-icon><br />\n          <ion-text>Wish List</ion-text>\n        </ion-col>\n        <ion-col>\n          <ion-icon name=\"checkbox-outline\" style=\"zoom:1.5;\"></ion-icon><br />\n          Orders\n        </ion-col>\n        <ion-col>\n          <ion-icon name=\"cart\" style=\"zoom:1.5;\"></ion-icon><br />\n          Cart\n        </ion-col>\n        <ion-col>\n          <ion-icon name=\"gift\" style=\"zoom:1.5;\"></ion-icon><br />\n          Deals\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <!-- Some Orders Option -->\n  <ion-card>\n    <ion-item-group>\n      <ion-item-divider>\n        <ion-label>Orders</ion-label>\n      </ion-item-divider>\n\n      <ion-item lines=\"none\">\n        <ion-label>Unpaid</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>To be shipped</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>Shipped</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>To be reviewed</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>In dispute</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n    </ion-item-group>\n  </ion-card>\n\n  <ion-card>\n    <ion-item-group>\n      <ion-item lines=\"none\">\n        <ion-label>Wallet</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>Shipping Address</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>Redeem invite code</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>Questions & Answers</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n    </ion-item-group>\n  </ion-card>\n\n  <ion-card>\n    <ion-item-group>\n      <ion-item lines=\"none\">\n        <ion-label>Settings</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>App Suggestion</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-label>Help Center</ion-label>\n        <ion-icon name=\"arrow-forward\" size=\"small\" slot=\"end\"></ion-icon>\n      </ion-item>\n    </ion-item-group>\n  </ion-card>\n</ion-content>");

/***/ }),

/***/ 4073:
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n\n<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <!-- Back To Landing Page -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"color1\" default-href=\"\" text=\"\" icon=\"arrow-back\" routerLink=\"/landing\"\n        routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content id=\"signup\" fullscreen>\n  <div class=\"section_one\">\n    <h2>SIGN UP</h2>\n    <p>Create Anonymous Account</p>\n  </div>\n\n  <!-- SignUp Form -->\n  <div class=\"section_two ion-padding\">\n    <form class=\"ion-padding\">\n      <!-- Name -->\n      <!-- <ion-item>\n        <ion-label>\n          <ion-icon name=\"person\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"Name\" name=\"name\"></ion-input>\n      </ion-item> -->\n\n      <!-- Email -->\n      <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"mail\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"email\" placeholder=\"Email verified\" name=\"email\"></ion-input>\n      </ion-item>\n\n      <!-- Select password -->\n      <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"eye\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"password\" placeholder=\"Select password\" name=\"password\"></ion-input>\n      </ion-item>\n\n      <!-- Confirm password -->\n      <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"lock-closed-sharp\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"password\" placeholder=\"Confirm password\" name=\"confirm_password\"></ion-input>\n      </ion-item>\n\n      <!-- Country -->\n      <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"Country\" name=\"country\"></ion-input>\n      </ion-item>\n\n       <!-- State -->\n       <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"State\" name=\"state\"></ion-input>\n      </ion-item>\n\n      <!-- city -->\n      <ion-item class=\"ion-padding-top\">\n        <ion-label>\n          <ion-icon name=\"\" color=\"color2\"></ion-icon>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"City\" name=\"city\"></ion-input>\n      </ion-item>\n\n\n      <div class=\"section_second\">\n        <h2 >Lets do good and prosper!</h2>\n      </div>\n\n      \n\n      <!-- SignUp Button -->\n      <div class=\"buttons ion-padding-top\">\n        <ion-button expand=\"block\" routerLink=\"/login\" routerDirection=\"root\">\n          <ion-text><strong>SIGN UP</strong></ion-text>\n        </ion-button>\n      </div>\n    </form>\n  </div>\n</ion-content>");

/***/ }),

/***/ 3330:
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/review-info/review-info.component.html ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header [translucent]=\"true\" class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n\n    <!-- Title of the app -->\n    <ion-title> Your Information</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content  class=\"ion-padding std-from\" id=\"home\" fullscreen>\n  <div class=\"section_one\">\n    <h4>Here is your information:</h4>\n    <p >It is saved as a draft...</p>\n    <p>You can continue from where you left off. </p>\n  </div>\n\n  <br><br>\n  <p>Message for family:</p> \n  <ion-list>\n    <ion-item class=\"\">\n      <ion-label>\n        <ion-icon name=\"\" color=\"color2\"></ion-icon>\n      </ion-label>\n      <ion-textarea rows=\"5\" [(ngModel)]=\"message\" placeholder=\"Start Typing Here...\" name=\"message\"></ion-textarea>\n    </ion-item>\n    \n    <ion-button expand=\"block\" (click)=\"rewardOption()\" routerDirection=\"root\">\n      share & select reward\n    </ion-button>\n\n    <br><br>\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class=\"ion-no-border std-from\">\n  <ion-list lines=\"none\">\n\n    <!-- <ion-button expand=\"block\" routerDirection=\"root\">\n      <ion-text><strong>Review Info</strong></ion-text>\n    </ion-button> -->\n\n  </ion-list>\n</ion-footer>\n");

/***/ }),

/***/ 4807:
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/reward-option/reward-option.component.html ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header [translucent]=\"true\" class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n\n    <!-- Title of the app -->\n    <ion-title> Reward Option </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content  class=\"ion-padding std-from\" fullscreen>\n  <div class=\"section_one\">\n    <h4>Select your reward option!</h4>\n    <p>You can select your reward preference from down below</p>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div class=\"box\">Amazon</div>\n      </ion-col>\n      <ion-col>\n        <div class=\"box\">Flipkart</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class=\"box\">Mintra</div>\n      </ion-col>\n      <ion-col>\n        <div class=\"box\">Nykaa</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div class=\"box\">Lenskart</div>\n      </ion-col>\n      <ion-col>\n        <div class=\"box\">Paytm</div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-list>\n    <ion-button expand=\"block\" (click)=\"confirmInfo()\">Confirm Information</ion-button>\n    <br><br>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ 9378:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/search-children/search-children.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header [translucent]=\"true\" class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n\n    <!-- Title of the app -->\n    <ion-title>Search Children</ion-title>\n\n    <!-- Cart Button Option -->\n    <!-- <ion-buttons slot=\"end\">\n      <ion-button color=\"white\" (click)=\"gotoCartPage()\">\n        <ion-icon name=\"cart\"></ion-icon>\n      </ion-button>\n    </ion-buttons> -->\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content  class=\"ion-padding\" id=\"home\" fullscreen>\n\n  <div>\n    <ngx-rating [(ngModel)]=\"rating\" (ngModelChange)=\"change_callback($event)\"></ngx-rating>\n  </div> \n  <!-- Children Name -->\n  <ion-item class=\"\">\n    <ion-input type=\"text\" placeholder=\"Children Name / Email / Phone\" name=\"children_name\" [(ngModel)]=\"searchTxt\" (ionChange)=\"searchChildren()\"></ion-input>\n  </ion-item>\n\n  <ion-list>\n    <ion-list-header>\n      Search Result\n    </ion-list-header>\n\n    <ion-item *ngFor=\"let item of friendsInfo$ | async\" (click)=\"detailPopup(item.id)\">\n      <ion-label>\n        <h2>{{ item.name }}</h2>\n        <h3>{{ item.phone }}</h3>\n        <h3>{{ item.email }}</h3>\n        <p>{{ item.country +', '+item.state+', '+item.city }}</p>\n      </ion-label>\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n");

/***/ }),

/***/ 8232:
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/submission-confirmatoin/submission-confirmatoin.component.html ***!
  \****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n      <!-- Side Menu Option -->\n      <ion-buttons slot=\"start\">\n          <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n\n      <!-- Title of page -->\n      <ion-title>Submission Confirmation</ion-title>\n\n  </ion-toolbar>\n</ion-header>\n\n\n<!-- Contents -->\n<ion-content class=\"ion-padding std-from\">\n\n  <br>\n  <ion-label><span>That's all.</span></ion-label>\n  <p>Your information has been shared anonymously.</p>\n\n  <br>\n  <ion-label><span>What can you expect next?</span></ion-label>\n\n  <p>1) You will receive an alert if and when information is matched to your friend's family member.  Could be seconds to never.  </p>\n\n  <p>2) When they access the information, you will receive an alert and your reward.  </p>\n\n  <p>3)  You will receive a second alert for feedback rating, if and when your friend's family member rates your shared information.  Based on your rating you could expect addition, subtraction or no change to your cash reward value.  </p>\n\n  <p>4) If you don't receive any alert within 72 hours, your friend's family member does not have an account with Campus Anonymous.  Don't give up yet.  Maybe they have never heard of Campus Anonymous and you may have a discreet way of bringing it to their attention.   </p>\n\n</ion-content>\n");

/***/ }),

/***/ 5282:
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/terms/terms.component.html ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n\n\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\" routerDirection=\"root\">\n      </ion-back-button>\n    </ion-buttons>\n\n    <!-- Page tile -->\n    <ion-title>Terms of Use</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Main Contents -->\n<ion-content id=\"category\" class=\"ion-padding\">\n\n</ion-content>\n");

/***/ }),

/***/ 8697:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update-password/update-password.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n    <!-- Side Menu Option -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n\n    <ion-title>Reset Password</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"section_second ion-padding\" fullscreen=\"true\">\n  <div class=\"page-half-bottom form-list\">\n    <div class=\"field-content\">\n      <form class=\"ion-padding std-from\" [formGroup]=\"ChangeForm\">\n        <ion-list lines=\"none\">\n\n            <!-- New Password -->\n            <ion-list-header>\n              <ion-label>New Password <sup>*</sup></ion-label>\n            </ion-list-header>\n            <ion-item lines=\"none\">\n              <ion-input type=\"password\" placeholder=\"New Password\" name=\"new_password\" formControlName=\"new_password\"></ion-input>\n            </ion-item>\n\n            <!-- New Confirm Password -->\n            <ion-list-header>\n              <ion-label>Confirm Password <sup>*</sup></ion-label>\n            </ion-list-header>\n            <ion-item lines=\"none\">\n              <ion-input type=\"password\" placeholder=\"New Confirm Password\" name=\"new_confirm_password\" formControlName=\"new_confirm_password\"></ion-input>\n            </ion-item>\n\n        </ion-list>\n      </form>\n    </div>\n  </div>\n\n</ion-content>\n\n<ion-footer>\n  <div class=\"std-from\">\n    <ion-list lines=\"none\">\n      <ion-button expand=\"block\" (click)=\"change()\"> Update <i class=\"flaticon-right-arrow\"></i> </ion-button>\n    </ion-list>\n  </div>\n</ion-footer>");

/***/ }),

/***/ 492:
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/verify-otp/verify-otp.component.html ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- Header -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <!-- Back To Landing Page -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button default-href=\"\" text=\"\" icon=\"arrow-back\">\n      </ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<!-- Contents -->\n<ion-content id=\"verification\" fullscreen>\n  <div class=\"section_one ion-padding std-from\">\n    <h4>Enter Verification Code</h4>\n    <p>Enter the verification code we just send you on your phone number</p>\n  </div>\n\n  <div class=\"section_two ion-padding std-from\">\n\n    <!-- <img class=\"ion-padding\" src=\"assets/images/otp.jpg\" /> -->\n\n    <!-- Enter Verification Code -->\n      <ion-list lines=\"none\">\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-input [(ngModel)]=\"one\" ngDefaultControl placeholder=\"\" maxlength=\"1\" #text1 (keyup)=\"clickEvent('text1', text1)\" text-center></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-input [(ngModel)]=\"two\" ngDefaultControl placeholder=\"\" maxlength=\"1\" #text2 (keyup)=\"clickEvent('text2', text2)\" text-center></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-input [(ngModel)]=\"three\" ngDefaultControl placeholder=\"\" maxlength=\"1\" #text3 (keyup)=\"clickEvent('text3', text3)\" text-center></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-input [(ngModel)]=\"four\" ngDefaultControl placeholder=\"\" maxlength=\"1\" #text4 text-center></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <!-- Resend Varification Code Option Button -->\n      <ion-list-header class=\"link ion-text-center\" routerLink=\"/login\">\n        <ion-label>If you dont receive the code ? <span (click)=\"sendOtp()\">Resend</span></ion-label>\n      </ion-list-header>\n\n      <ion-button expand=\"block\" (click)=\"verify()\">Verify</ion-button>\n    </ion-list>\n\n    <!-- <ion-button fill=\"clear\" >\n      <span>If you dont receive the code ?</span>\n      <ion-text color=\"color1\" (click)=\"sendOtp()\"><strong>RESEND</strong></ion-text>\n    </ion-button> -->\n\n  </div>\n</ion-content>\n\n");

/***/ }),

/***/ 6226:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/welcome/landing-page.component.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content class=\"ion-no-padding auth-page\" fullscreen>\n\n  <div class=\"header\">\n    <h2>\n      Campus Anonymous!</h2>\n    <h3>Register</h3>\n  </div>\n\n  <div class=\"std-from\">\n    <ion-list lines=\"none\">\n      <ion-list-header>\n        Sign in to help a friend and collect your reward\n      </ion-list-header>\n\n      <!-- <ion-list-header>\n        <ion-label>Register As<sup>*</sup></ion-label>\n      </ion-list-header> -->\n      <ion-item>\n        <ion-select [(ngModel)]=\"type\">\n          <ion-select-option value=\"parent\">Family</ion-select-option>\n          <ion-select-option value=\"child\">Student</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <!-- <ion-list-header>\n        <ion-label>Name<sup>*</sup></ion-label>\n      </ion-list-header> -->\n      <ion-item>\n        <ion-input type=\"text\" [(ngModel)]=\"name\" placeholder=\"Name\" name=\"name\"></ion-input>\n      </ion-item>\n\n      <!-- <ion-list-header>\n        <ion-label>Mobile<sup>*</sup></ion-label>\n      </ion-list-header> -->\n      <ion-item>\n        <ion-input type=\"text\" [(ngModel)]=\"mobile\" placeholder=\"Mobile Number\" name=\"mobile\"></ion-input>\n      </ion-item>\n\n      <!-- <ion-list-header>\n        <ion-label>Email<sup>*</sup></ion-label>\n      </ion-list-header> -->\n      <ion-item>\n        <ion-input type=\"email\" [(ngModel)]=\"email\" placeholder=\"Email\" name=\"email\"></ion-input>\n      </ion-item>\n\n      <ion-button expand=\"block\" (click)=\"submit()\">Submit</ion-button>\n\n      <ion-list-header class=\"link ion-text-center\" routerLink=\"/login\">\n        <ion-label>Already have an account? <span>Login</span></ion-label>\n      </ion-list-header>\n\n      <ion-list-header routerLink=\"/how-it-works\" class=\"link ion-text-center\">\n        <ion-label><span>How It Works?</span></ion-label>\n      </ion-list-header>\n\n    </ion-list>\n  </div>\n</ion-content>\n");

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map